package br.comperve.xtext.candidato_import.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPlan2CandLexer extends Lexer {
    public static final int RULE_COLUNA_BANCO=5;
    public static final int RULE_EMAIL=8;
    public static final int RULE_STRING=11;
    public static final int RULE_SL_COMMENT=13;
    public static final int T__19=19;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int RULE_NOME=9;
    public static final int EOF=-1;
    public static final int RULE_ID=10;
    public static final int RULE_WS=14;
    public static final int RULE_CPF_FORMATADO=7;
    public static final int RULE_ANY_OTHER=15;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=12;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int RULE_SEPARADOR=6;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators

    public InternalPlan2CandLexer() {;} 
    public InternalPlan2CandLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalPlan2CandLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalPlan2Cand.g"; }

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:11:7: ( 'BEGIN_MAPEAMENTO' )
            // InternalPlan2Cand.g:11:9: 'BEGIN_MAPEAMENTO'
            {
            match("BEGIN_MAPEAMENTO"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:12:7: ( 'END_MAPEAMENTO' )
            // InternalPlan2Cand.g:12:9: 'END_MAPEAMENTO'
            {
            match("END_MAPEAMENTO"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:13:7: ( 'BEGIN_DADOS' )
            // InternalPlan2Cand.g:13:9: 'BEGIN_DADOS'
            {
            match("BEGIN_DADOS"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:14:7: ( 'END_DADOS' )
            // InternalPlan2Cand.g:14:9: 'END_DADOS'
            {
            match("END_DADOS"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:15:7: ( 'coluna' )
            // InternalPlan2Cand.g:15:9: 'coluna'
            {
            match("coluna"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:16:7: ( ':' )
            // InternalPlan2Cand.g:16:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:17:7: ( 'destino' )
            // InternalPlan2Cand.g:17:9: 'destino'
            {
            match("destino"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:18:7: ( '=' )
            // InternalPlan2Cand.g:18:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:19:7: ( ';' )
            // InternalPlan2Cand.g:19:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:20:7: ( ',' )
            // InternalPlan2Cand.g:20:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:21:7: ( 'maiusculo' )
            // InternalPlan2Cand.g:21:9: 'maiusculo'
            {
            match("maiusculo"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:22:7: ( 'minusculo' )
            // InternalPlan2Cand.g:22:9: 'minusculo'
            {
            match("minusculo"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:23:7: ( 'apenas_numeros' )
            // InternalPlan2Cand.g:23:9: 'apenas_numeros'
            {
            match("apenas_numeros"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "RULE_NOME"
    public final void mRULE_NOME() throws RecognitionException {
        try {
            int _type = RULE_NOME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:456:11: ( '\\'' ( 'a' .. 'z' | 'A' .. 'Z' | ' ' ) ( 'a' .. 'z' | 'A' .. 'Z' | ' ' )* '\\'' )
            // InternalPlan2Cand.g:456:13: '\\'' ( 'a' .. 'z' | 'A' .. 'Z' | ' ' ) ( 'a' .. 'z' | 'A' .. 'Z' | ' ' )* '\\''
            {
            match('\''); 
            if ( input.LA(1)==' '||(input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalPlan2Cand.g:456:42: ( 'a' .. 'z' | 'A' .. 'Z' | ' ' )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==' '||(LA1_0>='A' && LA1_0<='Z')||(LA1_0>='a' && LA1_0<='z')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPlan2Cand.g:
            	    {
            	    if ( input.LA(1)==' '||(input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NOME"

    // $ANTLR start "RULE_EMAIL"
    public final void mRULE_EMAIL() throws RecognitionException {
        try {
            int _type = RULE_EMAIL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:458:12: ( '\\'' ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '.' | '_' )* '@' ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '.' | '_' )* '\\'' )
            // InternalPlan2Cand.g:458:14: '\\'' ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '.' | '_' )* '@' ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '.' | '_' )* '\\''
            {
            match('\''); 
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalPlan2Cand.g:458:39: ( 'a' .. 'z' | 'A' .. 'Z' | '.' | '_' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0=='.'||(LA2_0>='A' && LA2_0<='Z')||LA2_0=='_'||(LA2_0>='a' && LA2_0<='z')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalPlan2Cand.g:
            	    {
            	    if ( input.LA(1)=='.'||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            match('@'); 
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalPlan2Cand.g:458:92: ( 'a' .. 'z' | 'A' .. 'Z' | '.' | '_' )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0=='.'||(LA3_0>='A' && LA3_0<='Z')||LA3_0=='_'||(LA3_0>='a' && LA3_0<='z')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalPlan2Cand.g:
            	    {
            	    if ( input.LA(1)=='.'||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_EMAIL"

    // $ANTLR start "RULE_CPF_FORMATADO"
    public final void mRULE_CPF_FORMATADO() throws RecognitionException {
        try {
            int _type = RULE_CPF_FORMATADO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:460:20: ( '0' .. '9' '0' .. '9' '0' .. '9' '.' '0' .. '9' '0' .. '9' '0' .. '9' '.' '0' .. '9' '0' .. '9' '0' .. '9' '-' '0' .. '9' '0' .. '9' )
            // InternalPlan2Cand.g:460:22: '0' .. '9' '0' .. '9' '0' .. '9' '.' '0' .. '9' '0' .. '9' '0' .. '9' '.' '0' .. '9' '0' .. '9' '0' .. '9' '-' '0' .. '9' '0' .. '9'
            {
            matchRange('0','9'); 
            matchRange('0','9'); 
            matchRange('0','9'); 
            match('.'); 
            matchRange('0','9'); 
            matchRange('0','9'); 
            matchRange('0','9'); 
            match('.'); 
            matchRange('0','9'); 
            matchRange('0','9'); 
            matchRange('0','9'); 
            match('-'); 
            matchRange('0','9'); 
            matchRange('0','9'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CPF_FORMATADO"

    // $ANTLR start "RULE_SEPARADOR"
    public final void mRULE_SEPARADOR() throws RecognitionException {
        try {
            int _type = RULE_SEPARADOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:462:16: ( '\\t' )
            // InternalPlan2Cand.g:462:18: '\\t'
            {
            match('\t'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SEPARADOR"

    // $ANTLR start "RULE_COLUNA_BANCO"
    public final void mRULE_COLUNA_BANCO() throws RecognitionException {
        try {
            int _type = RULE_COLUNA_BANCO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:464:19: ( ( 'a' .. 'z' | '_' )+ )
            // InternalPlan2Cand.g:464:21: ( 'a' .. 'z' | '_' )+
            {
            // InternalPlan2Cand.g:464:21: ( 'a' .. 'z' | '_' )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0=='_'||(LA4_0>='a' && LA4_0<='z')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalPlan2Cand.g:
            	    {
            	    if ( input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COLUNA_BANCO"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:466:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalPlan2Cand.g:466:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalPlan2Cand.g:466:11: ( '^' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='^') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalPlan2Cand.g:466:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalPlan2Cand.g:466:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='0' && LA6_0<='9')||(LA6_0>='A' && LA6_0<='Z')||LA6_0=='_'||(LA6_0>='a' && LA6_0<='z')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalPlan2Cand.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:468:10: ( ( '0' .. '9' )+ )
            // InternalPlan2Cand.g:468:12: ( '0' .. '9' )+
            {
            // InternalPlan2Cand.g:468:12: ( '0' .. '9' )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>='0' && LA7_0<='9')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalPlan2Cand.g:468:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:470:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalPlan2Cand.g:470:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalPlan2Cand.g:470:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='\"') ) {
                alt10=1;
            }
            else if ( (LA10_0=='\'') ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalPlan2Cand.g:470:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalPlan2Cand.g:470:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop8:
                    do {
                        int alt8=3;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0=='\\') ) {
                            alt8=1;
                        }
                        else if ( ((LA8_0>='\u0000' && LA8_0<='!')||(LA8_0>='#' && LA8_0<='[')||(LA8_0>=']' && LA8_0<='\uFFFF')) ) {
                            alt8=2;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalPlan2Cand.g:470:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalPlan2Cand.g:470:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalPlan2Cand.g:470:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalPlan2Cand.g:470:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop9:
                    do {
                        int alt9=3;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0=='\\') ) {
                            alt9=1;
                        }
                        else if ( ((LA9_0>='\u0000' && LA9_0<='&')||(LA9_0>='(' && LA9_0<='[')||(LA9_0>=']' && LA9_0<='\uFFFF')) ) {
                            alt9=2;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalPlan2Cand.g:470:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalPlan2Cand.g:470:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:472:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalPlan2Cand.g:472:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalPlan2Cand.g:472:24: ( options {greedy=false; } : . )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0=='*') ) {
                    int LA11_1 = input.LA(2);

                    if ( (LA11_1=='/') ) {
                        alt11=2;
                    }
                    else if ( ((LA11_1>='\u0000' && LA11_1<='.')||(LA11_1>='0' && LA11_1<='\uFFFF')) ) {
                        alt11=1;
                    }


                }
                else if ( ((LA11_0>='\u0000' && LA11_0<=')')||(LA11_0>='+' && LA11_0<='\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalPlan2Cand.g:472:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:474:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalPlan2Cand.g:474:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalPlan2Cand.g:474:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>='\u0000' && LA12_0<='\t')||(LA12_0>='\u000B' && LA12_0<='\f')||(LA12_0>='\u000E' && LA12_0<='\uFFFF')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalPlan2Cand.g:474:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            // InternalPlan2Cand.g:474:40: ( ( '\\r' )? '\\n' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0=='\n'||LA14_0=='\r') ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalPlan2Cand.g:474:41: ( '\\r' )? '\\n'
                    {
                    // InternalPlan2Cand.g:474:41: ( '\\r' )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0=='\r') ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // InternalPlan2Cand.g:474:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:476:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalPlan2Cand.g:476:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalPlan2Cand.g:476:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt15=0;
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>='\t' && LA15_0<='\n')||LA15_0=='\r'||LA15_0==' ') ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalPlan2Cand.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt15 >= 1 ) break loop15;
                        EarlyExitException eee =
                            new EarlyExitException(15, input);
                        throw eee;
                }
                cnt15++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalPlan2Cand.g:478:16: ( . )
            // InternalPlan2Cand.g:478:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalPlan2Cand.g:1:8: ( T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | RULE_NOME | RULE_EMAIL | RULE_CPF_FORMATADO | RULE_SEPARADOR | RULE_COLUNA_BANCO | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt16=25;
        alt16 = dfa16.predict(input);
        switch (alt16) {
            case 1 :
                // InternalPlan2Cand.g:1:10: T__16
                {
                mT__16(); 

                }
                break;
            case 2 :
                // InternalPlan2Cand.g:1:16: T__17
                {
                mT__17(); 

                }
                break;
            case 3 :
                // InternalPlan2Cand.g:1:22: T__18
                {
                mT__18(); 

                }
                break;
            case 4 :
                // InternalPlan2Cand.g:1:28: T__19
                {
                mT__19(); 

                }
                break;
            case 5 :
                // InternalPlan2Cand.g:1:34: T__20
                {
                mT__20(); 

                }
                break;
            case 6 :
                // InternalPlan2Cand.g:1:40: T__21
                {
                mT__21(); 

                }
                break;
            case 7 :
                // InternalPlan2Cand.g:1:46: T__22
                {
                mT__22(); 

                }
                break;
            case 8 :
                // InternalPlan2Cand.g:1:52: T__23
                {
                mT__23(); 

                }
                break;
            case 9 :
                // InternalPlan2Cand.g:1:58: T__24
                {
                mT__24(); 

                }
                break;
            case 10 :
                // InternalPlan2Cand.g:1:64: T__25
                {
                mT__25(); 

                }
                break;
            case 11 :
                // InternalPlan2Cand.g:1:70: T__26
                {
                mT__26(); 

                }
                break;
            case 12 :
                // InternalPlan2Cand.g:1:76: T__27
                {
                mT__27(); 

                }
                break;
            case 13 :
                // InternalPlan2Cand.g:1:82: T__28
                {
                mT__28(); 

                }
                break;
            case 14 :
                // InternalPlan2Cand.g:1:88: RULE_NOME
                {
                mRULE_NOME(); 

                }
                break;
            case 15 :
                // InternalPlan2Cand.g:1:98: RULE_EMAIL
                {
                mRULE_EMAIL(); 

                }
                break;
            case 16 :
                // InternalPlan2Cand.g:1:109: RULE_CPF_FORMATADO
                {
                mRULE_CPF_FORMATADO(); 

                }
                break;
            case 17 :
                // InternalPlan2Cand.g:1:128: RULE_SEPARADOR
                {
                mRULE_SEPARADOR(); 

                }
                break;
            case 18 :
                // InternalPlan2Cand.g:1:143: RULE_COLUNA_BANCO
                {
                mRULE_COLUNA_BANCO(); 

                }
                break;
            case 19 :
                // InternalPlan2Cand.g:1:161: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 20 :
                // InternalPlan2Cand.g:1:169: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 21 :
                // InternalPlan2Cand.g:1:178: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 22 :
                // InternalPlan2Cand.g:1:190: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 23 :
                // InternalPlan2Cand.g:1:206: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 24 :
                // InternalPlan2Cand.g:1:222: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 25 :
                // InternalPlan2Cand.g:1:230: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA16 dfa16 = new DFA16(this);
    static final String DFA16_eotS =
        "\1\uffff\2\26\1\32\1\uffff\1\32\3\uffff\2\32\1\24\1\47\1\50\1\32\1\24\1\uffff\2\24\2\uffff\1\26\1\uffff\1\26\2\32\2\uffff\1\32\3\uffff\3\32\3\uffff\1\47\5\uffff\2\26\5\32\5\uffff\1\47\2\26\5\32\3\uffff\3\26\5\32\2\uffff\3\26\1\132\4\32\1\uffff\4\26\1\uffff\1\143\3\32\4\26\1\uffff\3\32\3\26\1\161\1\162\1\163\1\32\3\26\3\uffff\1\32\1\26\1\172\1\26\1\32\1\26\1\uffff\1\26\1\32\2\26\1\32\1\26\1\u0084\1\u0085\1\26\2\uffff\1\u0087\1\uffff";
    static final String DFA16_eofS =
        "\u0088\uffff";
    static final String DFA16_minS =
        "\1\0\1\105\1\116\1\60\1\uffff\1\60\3\uffff\2\60\1\0\1\60\1\11\1\60\1\101\1\uffff\1\0\1\52\2\uffff\1\107\1\uffff\1\104\2\60\2\uffff\1\60\3\uffff\3\60\2\0\1\uffff\1\60\5\uffff\1\111\1\137\5\60\1\uffff\4\0\1\56\1\116\1\104\5\60\1\uffff\1\0\1\uffff\1\137\2\101\5\60\1\uffff\1\0\1\104\1\120\1\104\5\60\1\uffff\2\101\1\105\1\117\1\uffff\4\60\1\120\1\104\1\101\1\123\1\uffff\3\60\1\105\1\117\1\115\4\60\1\101\1\123\1\105\3\uffff\1\60\1\115\1\60\1\116\1\60\1\105\1\uffff\1\124\1\60\1\116\1\117\1\60\1\124\2\60\1\117\2\uffff\1\60\1\uffff";
    static final String DFA16_maxS =
        "\1\uffff\1\105\1\116\1\172\1\uffff\1\172\3\uffff\2\172\1\uffff\1\71\1\40\2\172\1\uffff\1\uffff\1\57\2\uffff\1\107\1\uffff\1\104\2\172\2\uffff\1\172\3\uffff\3\172\2\uffff\1\uffff\1\71\5\uffff\1\111\1\137\5\172\1\uffff\4\uffff\1\56\1\116\1\115\5\172\1\uffff\1\uffff\1\uffff\1\137\2\101\5\172\1\uffff\1\uffff\1\115\1\120\1\104\5\172\1\uffff\2\101\1\105\1\117\1\uffff\4\172\1\120\1\104\1\101\1\123\1\uffff\3\172\1\105\1\117\1\115\4\172\1\101\1\123\1\105\3\uffff\1\172\1\115\1\172\1\116\1\172\1\105\1\uffff\1\124\1\172\1\116\1\117\1\172\1\124\2\172\1\117\2\uffff\1\172\1\uffff";
    static final String DFA16_acceptS =
        "\4\uffff\1\6\1\uffff\1\10\1\11\1\12\7\uffff\1\23\2\uffff\1\30\1\31\1\uffff\1\23\3\uffff\1\22\1\6\1\uffff\1\10\1\11\1\12\5\uffff\1\25\1\uffff\1\24\1\21\1\30\1\26\1\27\7\uffff\1\16\14\uffff\1\16\1\uffff\1\20\10\uffff\1\17\11\uffff\1\17\4\uffff\1\5\10\uffff\1\7\15\uffff\1\4\1\13\1\14\6\uffff\1\3\11\uffff\1\2\1\15\1\uffff\1\1";
    static final String DFA16_specialS =
        "\1\4\12\uffff\1\1\5\uffff\1\12\21\uffff\1\6\1\7\17\uffff\1\3\1\2\1\11\1\10\11\uffff\1\0\12\uffff\1\5\73\uffff}>";
    static final String[] DFA16_transitionS = {
            "\11\24\1\15\1\23\2\24\1\23\22\24\1\23\1\24\1\21\4\24\1\13\4\24\1\10\2\24\1\22\12\14\1\4\1\7\1\24\1\6\3\24\1\20\1\1\2\20\1\2\25\20\3\24\1\17\1\16\1\24\1\12\1\16\1\3\1\5\10\16\1\11\15\16\uff85\24",
            "\1\25",
            "\1\27",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\16\31\1\30\13\31",
            "",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\4\31\1\34\25\31",
            "",
            "",
            "",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\1\40\7\31\1\41\21\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\17\31\1\42\12\31",
            "\40\45\1\44\40\45\32\43\6\45\32\43\uff85\45",
            "\12\46",
            "\2\51\2\uffff\1\51\22\uffff\1\51",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\32\31",
            "\32\26\4\uffff\1\26\1\uffff\32\26",
            "",
            "\0\45",
            "\1\52\4\uffff\1\53",
            "",
            "",
            "\1\54",
            "",
            "\1\55",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\13\31\1\56\16\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\32\31",
            "",
            "",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\22\31\1\57\7\31",
            "",
            "",
            "",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\10\31\1\60\21\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\15\31\1\61\14\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\4\31\1\62\25\31",
            "\40\45\1\66\6\45\1\63\6\45\1\65\21\45\1\67\32\64\4\45\1\65\1\45\32\64\uff85\45",
            "\40\45\1\66\6\45\1\63\31\45\32\66\6\45\32\66\uff85\45",
            "",
            "\12\70",
            "",
            "",
            "",
            "",
            "",
            "\1\71",
            "\1\72",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\24\31\1\73\5\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\23\31\1\74\6\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\24\31\1\75\5\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\24\31\1\76\5\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\15\31\1\77\14\31",
            "",
            "\40\45\1\66\6\45\1\63\6\45\1\65\21\45\1\67\32\64\4\45\1\65\1\45\32\64\uff85\45",
            "\56\45\1\65\21\45\1\67\32\65\4\45\1\65\1\45\32\65\uff85\45",
            "\40\45\1\66\6\45\1\63\31\45\32\66\6\45\32\66\uff85\45",
            "\101\45\32\101\6\45\32\101\uff85\45",
            "\1\102",
            "\1\103",
            "\1\105\10\uffff\1\104",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\15\31\1\106\14\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\10\31\1\107\21\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\22\31\1\110\7\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\22\31\1\111\7\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\1\112\31\31",
            "",
            "\47\45\1\113\6\45\1\114\22\45\32\114\4\45\1\114\1\45\32\114\uff85\45",
            "",
            "\1\115",
            "\1\116",
            "\1\117",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\1\120\31\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\15\31\1\121\14\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\2\31\1\122\27\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\2\31\1\123\27\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\22\31\1\124\7\31",
            "",
            "\47\45\1\113\6\45\1\114\22\45\32\114\4\45\1\114\1\45\32\114\uff85\45",
            "\1\127\10\uffff\1\126",
            "\1\130",
            "\1\131",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\32\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\16\31\1\133\13\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\24\31\1\134\5\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\24\31\1\135\5\31",
            "\12\26\7\uffff\32\26\4\uffff\1\136\1\uffff\32\31",
            "",
            "\1\137",
            "\1\140",
            "\1\141",
            "\1\142",
            "",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\32\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\13\31\1\144\16\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\13\31\1\145\16\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\15\31\1\146\14\31",
            "\1\147",
            "\1\150",
            "\1\151",
            "\1\152",
            "",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\16\31\1\153\13\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\16\31\1\154\13\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\24\31\1\155\5\31",
            "\1\156",
            "\1\157",
            "\1\160",
            "\12\26\7\uffff\32\26\4\uffff\1\26\1\uffff\32\26",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\32\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\32\31",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\14\31\1\164\15\31",
            "\1\165",
            "\1\166",
            "\1\167",
            "",
            "",
            "",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\4\31\1\170\25\31",
            "\1\171",
            "\12\26\7\uffff\32\26\4\uffff\1\26\1\uffff\32\26",
            "\1\173",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\21\31\1\174\10\31",
            "\1\175",
            "",
            "\1\176",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\16\31\1\177\13\31",
            "\1\u0080",
            "\1\u0081",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\22\31\1\u0082\7\31",
            "\1\u0083",
            "\12\26\7\uffff\32\26\4\uffff\1\26\1\uffff\32\26",
            "\12\26\7\uffff\32\26\4\uffff\1\31\1\uffff\32\31",
            "\1\u0086",
            "",
            "",
            "\12\26\7\uffff\32\26\4\uffff\1\26\1\uffff\32\26",
            ""
    };

    static final short[] DFA16_eot = DFA.unpackEncodedString(DFA16_eotS);
    static final short[] DFA16_eof = DFA.unpackEncodedString(DFA16_eofS);
    static final char[] DFA16_min = DFA.unpackEncodedStringToUnsignedChars(DFA16_minS);
    static final char[] DFA16_max = DFA.unpackEncodedStringToUnsignedChars(DFA16_maxS);
    static final short[] DFA16_accept = DFA.unpackEncodedString(DFA16_acceptS);
    static final short[] DFA16_special = DFA.unpackEncodedString(DFA16_specialS);
    static final short[][] DFA16_transition;

    static {
        int numStates = DFA16_transitionS.length;
        DFA16_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA16_transition[i] = DFA.unpackEncodedString(DFA16_transitionS[i]);
        }
    }

    class DFA16 extends DFA {

        public DFA16(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 16;
            this.eot = DFA16_eot;
            this.eof = DFA16_eof;
            this.min = DFA16_min;
            this.max = DFA16_max;
            this.accept = DFA16_accept;
            this.special = DFA16_special;
            this.transition = DFA16_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | RULE_NOME | RULE_EMAIL | RULE_CPF_FORMATADO | RULE_SEPARADOR | RULE_COLUNA_BANCO | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA16_65 = input.LA(1);

                        s = -1;
                        if ( (LA16_65=='\'') ) {s = 75;}

                        else if ( ((LA16_65>='\u0000' && LA16_65<='&')||(LA16_65>='(' && LA16_65<='-')||(LA16_65>='/' && LA16_65<='@')||(LA16_65>='[' && LA16_65<='^')||LA16_65=='`'||(LA16_65>='{' && LA16_65<='\uFFFF')) ) {s = 37;}

                        else if ( (LA16_65=='.'||(LA16_65>='A' && LA16_65<='Z')||LA16_65=='_'||(LA16_65>='a' && LA16_65<='z')) ) {s = 76;}

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA16_11 = input.LA(1);

                        s = -1;
                        if ( ((LA16_11>='A' && LA16_11<='Z')||(LA16_11>='a' && LA16_11<='z')) ) {s = 35;}

                        else if ( (LA16_11==' ') ) {s = 36;}

                        else if ( ((LA16_11>='\u0000' && LA16_11<='\u001F')||(LA16_11>='!' && LA16_11<='@')||(LA16_11>='[' && LA16_11<='`')||(LA16_11>='{' && LA16_11<='\uFFFF')) ) {s = 37;}

                        else s = 20;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA16_53 = input.LA(1);

                        s = -1;
                        if ( ((LA16_53>='\u0000' && LA16_53<='-')||(LA16_53>='/' && LA16_53<='?')||(LA16_53>='[' && LA16_53<='^')||LA16_53=='`'||(LA16_53>='{' && LA16_53<='\uFFFF')) ) {s = 37;}

                        else if ( (LA16_53=='@') ) {s = 55;}

                        else if ( (LA16_53=='.'||(LA16_53>='A' && LA16_53<='Z')||LA16_53=='_'||(LA16_53>='a' && LA16_53<='z')) ) {s = 53;}

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA16_52 = input.LA(1);

                        s = -1;
                        if ( (LA16_52=='\'') ) {s = 51;}

                        else if ( ((LA16_52>='\u0000' && LA16_52<='\u001F')||(LA16_52>='!' && LA16_52<='&')||(LA16_52>='(' && LA16_52<='-')||(LA16_52>='/' && LA16_52<='?')||(LA16_52>='[' && LA16_52<='^')||LA16_52=='`'||(LA16_52>='{' && LA16_52<='\uFFFF')) ) {s = 37;}

                        else if ( ((LA16_52>='A' && LA16_52<='Z')||(LA16_52>='a' && LA16_52<='z')) ) {s = 52;}

                        else if ( (LA16_52=='@') ) {s = 55;}

                        else if ( (LA16_52=='.'||LA16_52=='_') ) {s = 53;}

                        else if ( (LA16_52==' ') ) {s = 54;}

                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA16_0 = input.LA(1);

                        s = -1;
                        if ( (LA16_0=='B') ) {s = 1;}

                        else if ( (LA16_0=='E') ) {s = 2;}

                        else if ( (LA16_0=='c') ) {s = 3;}

                        else if ( (LA16_0==':') ) {s = 4;}

                        else if ( (LA16_0=='d') ) {s = 5;}

                        else if ( (LA16_0=='=') ) {s = 6;}

                        else if ( (LA16_0==';') ) {s = 7;}

                        else if ( (LA16_0==',') ) {s = 8;}

                        else if ( (LA16_0=='m') ) {s = 9;}

                        else if ( (LA16_0=='a') ) {s = 10;}

                        else if ( (LA16_0=='\'') ) {s = 11;}

                        else if ( ((LA16_0>='0' && LA16_0<='9')) ) {s = 12;}

                        else if ( (LA16_0=='\t') ) {s = 13;}

                        else if ( (LA16_0=='_'||LA16_0=='b'||(LA16_0>='e' && LA16_0<='l')||(LA16_0>='n' && LA16_0<='z')) ) {s = 14;}

                        else if ( (LA16_0=='^') ) {s = 15;}

                        else if ( (LA16_0=='A'||(LA16_0>='C' && LA16_0<='D')||(LA16_0>='F' && LA16_0<='Z')) ) {s = 16;}

                        else if ( (LA16_0=='\"') ) {s = 17;}

                        else if ( (LA16_0=='/') ) {s = 18;}

                        else if ( (LA16_0=='\n'||LA16_0=='\r'||LA16_0==' ') ) {s = 19;}

                        else if ( ((LA16_0>='\u0000' && LA16_0<='\b')||(LA16_0>='\u000B' && LA16_0<='\f')||(LA16_0>='\u000E' && LA16_0<='\u001F')||LA16_0=='!'||(LA16_0>='#' && LA16_0<='&')||(LA16_0>='(' && LA16_0<='+')||(LA16_0>='-' && LA16_0<='.')||LA16_0=='<'||(LA16_0>='>' && LA16_0<='@')||(LA16_0>='[' && LA16_0<=']')||LA16_0=='`'||(LA16_0>='{' && LA16_0<='\uFFFF')) ) {s = 20;}

                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA16_76 = input.LA(1);

                        s = -1;
                        if ( (LA16_76=='\'') ) {s = 75;}

                        else if ( (LA16_76=='.'||(LA16_76>='A' && LA16_76<='Z')||LA16_76=='_'||(LA16_76>='a' && LA16_76<='z')) ) {s = 76;}

                        else if ( ((LA16_76>='\u0000' && LA16_76<='&')||(LA16_76>='(' && LA16_76<='-')||(LA16_76>='/' && LA16_76<='@')||(LA16_76>='[' && LA16_76<='^')||LA16_76=='`'||(LA16_76>='{' && LA16_76<='\uFFFF')) ) {s = 37;}

                        if ( s>=0 ) return s;
                        break;
                    case 6 : 
                        int LA16_35 = input.LA(1);

                        s = -1;
                        if ( (LA16_35=='\'') ) {s = 51;}

                        else if ( ((LA16_35>='\u0000' && LA16_35<='\u001F')||(LA16_35>='!' && LA16_35<='&')||(LA16_35>='(' && LA16_35<='-')||(LA16_35>='/' && LA16_35<='?')||(LA16_35>='[' && LA16_35<='^')||LA16_35=='`'||(LA16_35>='{' && LA16_35<='\uFFFF')) ) {s = 37;}

                        else if ( ((LA16_35>='A' && LA16_35<='Z')||(LA16_35>='a' && LA16_35<='z')) ) {s = 52;}

                        else if ( (LA16_35=='.'||LA16_35=='_') ) {s = 53;}

                        else if ( (LA16_35==' ') ) {s = 54;}

                        else if ( (LA16_35=='@') ) {s = 55;}

                        if ( s>=0 ) return s;
                        break;
                    case 7 : 
                        int LA16_36 = input.LA(1);

                        s = -1;
                        if ( (LA16_36=='\'') ) {s = 51;}

                        else if ( ((LA16_36>='\u0000' && LA16_36<='\u001F')||(LA16_36>='!' && LA16_36<='&')||(LA16_36>='(' && LA16_36<='@')||(LA16_36>='[' && LA16_36<='`')||(LA16_36>='{' && LA16_36<='\uFFFF')) ) {s = 37;}

                        else if ( (LA16_36==' '||(LA16_36>='A' && LA16_36<='Z')||(LA16_36>='a' && LA16_36<='z')) ) {s = 54;}

                        if ( s>=0 ) return s;
                        break;
                    case 8 : 
                        int LA16_55 = input.LA(1);

                        s = -1;
                        if ( ((LA16_55>='A' && LA16_55<='Z')||(LA16_55>='a' && LA16_55<='z')) ) {s = 65;}

                        else if ( ((LA16_55>='\u0000' && LA16_55<='@')||(LA16_55>='[' && LA16_55<='`')||(LA16_55>='{' && LA16_55<='\uFFFF')) ) {s = 37;}

                        if ( s>=0 ) return s;
                        break;
                    case 9 : 
                        int LA16_54 = input.LA(1);

                        s = -1;
                        if ( (LA16_54=='\'') ) {s = 51;}

                        else if ( ((LA16_54>='\u0000' && LA16_54<='\u001F')||(LA16_54>='!' && LA16_54<='&')||(LA16_54>='(' && LA16_54<='@')||(LA16_54>='[' && LA16_54<='`')||(LA16_54>='{' && LA16_54<='\uFFFF')) ) {s = 37;}

                        else if ( (LA16_54==' '||(LA16_54>='A' && LA16_54<='Z')||(LA16_54>='a' && LA16_54<='z')) ) {s = 54;}

                        if ( s>=0 ) return s;
                        break;
                    case 10 : 
                        int LA16_17 = input.LA(1);

                        s = -1;
                        if ( ((LA16_17>='\u0000' && LA16_17<='\uFFFF')) ) {s = 37;}

                        else s = 20;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 16, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}