package br.comperve.xtext.candidato_import.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import br.comperve.xtext.candidato_import.services.Plan2CandGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPlan2CandParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_COLUNA_BANCO", "RULE_SEPARADOR", "RULE_CPF_FORMATADO", "RULE_EMAIL", "RULE_NOME", "RULE_ID", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'BEGIN_MAPEAMENTO'", "'END_MAPEAMENTO'", "'BEGIN_DADOS'", "'END_DADOS'", "'coluna'", "':'", "'destino'", "'='", "';'", "','", "'maiusculo'", "'minusculo'", "'apenas_numeros'"
    };
    public static final int RULE_COLUNA_BANCO=5;
    public static final int RULE_EMAIL=8;
    public static final int RULE_STRING=11;
    public static final int RULE_SL_COMMENT=13;
    public static final int T__19=19;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int RULE_NOME=9;
    public static final int EOF=-1;
    public static final int RULE_ID=10;
    public static final int RULE_WS=14;
    public static final int RULE_CPF_FORMATADO=7;
    public static final int RULE_ANY_OTHER=15;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=12;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int RULE_SEPARADOR=6;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalPlan2CandParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPlan2CandParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPlan2CandParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPlan2Cand.g"; }



     	private Plan2CandGrammarAccess grammarAccess;

        public InternalPlan2CandParser(TokenStream input, Plan2CandGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected Plan2CandGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalPlan2Cand.g:64:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalPlan2Cand.g:64:46: (iv_ruleModel= ruleModel EOF )
            // InternalPlan2Cand.g:65:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalPlan2Cand.g:71:1: ruleModel returns [EObject current=null] : (otherlv_0= 'BEGIN_MAPEAMENTO' ( (lv_definicaoColunas_1_0= ruleDefinicaoColunas ) )+ otherlv_2= 'END_MAPEAMENTO' otherlv_3= 'BEGIN_DADOS' ( (lv_todosDados_4_0= ruleDados ) ) otherlv_5= 'END_DADOS' ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_definicaoColunas_1_0 = null;

        EObject lv_todosDados_4_0 = null;



        	enterRule();

        try {
            // InternalPlan2Cand.g:77:2: ( (otherlv_0= 'BEGIN_MAPEAMENTO' ( (lv_definicaoColunas_1_0= ruleDefinicaoColunas ) )+ otherlv_2= 'END_MAPEAMENTO' otherlv_3= 'BEGIN_DADOS' ( (lv_todosDados_4_0= ruleDados ) ) otherlv_5= 'END_DADOS' ) )
            // InternalPlan2Cand.g:78:2: (otherlv_0= 'BEGIN_MAPEAMENTO' ( (lv_definicaoColunas_1_0= ruleDefinicaoColunas ) )+ otherlv_2= 'END_MAPEAMENTO' otherlv_3= 'BEGIN_DADOS' ( (lv_todosDados_4_0= ruleDados ) ) otherlv_5= 'END_DADOS' )
            {
            // InternalPlan2Cand.g:78:2: (otherlv_0= 'BEGIN_MAPEAMENTO' ( (lv_definicaoColunas_1_0= ruleDefinicaoColunas ) )+ otherlv_2= 'END_MAPEAMENTO' otherlv_3= 'BEGIN_DADOS' ( (lv_todosDados_4_0= ruleDados ) ) otherlv_5= 'END_DADOS' )
            // InternalPlan2Cand.g:79:3: otherlv_0= 'BEGIN_MAPEAMENTO' ( (lv_definicaoColunas_1_0= ruleDefinicaoColunas ) )+ otherlv_2= 'END_MAPEAMENTO' otherlv_3= 'BEGIN_DADOS' ( (lv_todosDados_4_0= ruleDados ) ) otherlv_5= 'END_DADOS'
            {
            otherlv_0=(Token)match(input,16,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getModelAccess().getBEGIN_MAPEAMENTOKeyword_0());
            		
            // InternalPlan2Cand.g:83:3: ( (lv_definicaoColunas_1_0= ruleDefinicaoColunas ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==20) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPlan2Cand.g:84:4: (lv_definicaoColunas_1_0= ruleDefinicaoColunas )
            	    {
            	    // InternalPlan2Cand.g:84:4: (lv_definicaoColunas_1_0= ruleDefinicaoColunas )
            	    // InternalPlan2Cand.g:85:5: lv_definicaoColunas_1_0= ruleDefinicaoColunas
            	    {

            	    					newCompositeNode(grammarAccess.getModelAccess().getDefinicaoColunasDefinicaoColunasParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_definicaoColunas_1_0=ruleDefinicaoColunas();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"definicaoColunas",
            	    						lv_definicaoColunas_1_0,
            	    						"br.comperve.xtext.candidato_import.Plan2Cand.DefinicaoColunas");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            otherlv_2=(Token)match(input,17,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getModelAccess().getEND_MAPEAMENTOKeyword_2());
            		
            otherlv_3=(Token)match(input,18,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getModelAccess().getBEGIN_DADOSKeyword_3());
            		
            // InternalPlan2Cand.g:110:3: ( (lv_todosDados_4_0= ruleDados ) )
            // InternalPlan2Cand.g:111:4: (lv_todosDados_4_0= ruleDados )
            {
            // InternalPlan2Cand.g:111:4: (lv_todosDados_4_0= ruleDados )
            // InternalPlan2Cand.g:112:5: lv_todosDados_4_0= ruleDados
            {

            					newCompositeNode(grammarAccess.getModelAccess().getTodosDadosDadosParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_7);
            lv_todosDados_4_0=ruleDados();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getModelRule());
            					}
            					set(
            						current,
            						"todosDados",
            						lv_todosDados_4_0,
            						"br.comperve.xtext.candidato_import.Plan2Cand.Dados");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,19,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getModelAccess().getEND_DADOSKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleDados"
    // InternalPlan2Cand.g:137:1: entryRuleDados returns [EObject current=null] : iv_ruleDados= ruleDados EOF ;
    public final EObject entryRuleDados() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDados = null;


        try {
            // InternalPlan2Cand.g:137:46: (iv_ruleDados= ruleDados EOF )
            // InternalPlan2Cand.g:138:2: iv_ruleDados= ruleDados EOF
            {
             newCompositeNode(grammarAccess.getDadosRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDados=ruleDados();

            state._fsp--;

             current =iv_ruleDados; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDados"


    // $ANTLR start "ruleDados"
    // InternalPlan2Cand.g:144:1: ruleDados returns [EObject current=null] : ( (lv_dados_0_0= ruleLinhaDados ) )+ ;
    public final EObject ruleDados() throws RecognitionException {
        EObject current = null;

        EObject lv_dados_0_0 = null;



        	enterRule();

        try {
            // InternalPlan2Cand.g:150:2: ( ( (lv_dados_0_0= ruleLinhaDados ) )+ )
            // InternalPlan2Cand.g:151:2: ( (lv_dados_0_0= ruleLinhaDados ) )+
            {
            // InternalPlan2Cand.g:151:2: ( (lv_dados_0_0= ruleLinhaDados ) )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_INT||(LA2_0>=RULE_CPF_FORMATADO && LA2_0<=RULE_ID)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalPlan2Cand.g:152:3: (lv_dados_0_0= ruleLinhaDados )
            	    {
            	    // InternalPlan2Cand.g:152:3: (lv_dados_0_0= ruleLinhaDados )
            	    // InternalPlan2Cand.g:153:4: lv_dados_0_0= ruleLinhaDados
            	    {

            	    				newCompositeNode(grammarAccess.getDadosAccess().getDadosLinhaDadosParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_8);
            	    lv_dados_0_0=ruleLinhaDados();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getDadosRule());
            	    				}
            	    				add(
            	    					current,
            	    					"dados",
            	    					lv_dados_0_0,
            	    					"br.comperve.xtext.candidato_import.Plan2Cand.LinhaDados");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDados"


    // $ANTLR start "entryRuleDefinicaoColunas"
    // InternalPlan2Cand.g:173:1: entryRuleDefinicaoColunas returns [EObject current=null] : iv_ruleDefinicaoColunas= ruleDefinicaoColunas EOF ;
    public final EObject entryRuleDefinicaoColunas() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefinicaoColunas = null;


        try {
            // InternalPlan2Cand.g:173:57: (iv_ruleDefinicaoColunas= ruleDefinicaoColunas EOF )
            // InternalPlan2Cand.g:174:2: iv_ruleDefinicaoColunas= ruleDefinicaoColunas EOF
            {
             newCompositeNode(grammarAccess.getDefinicaoColunasRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDefinicaoColunas=ruleDefinicaoColunas();

            state._fsp--;

             current =iv_ruleDefinicaoColunas; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefinicaoColunas"


    // $ANTLR start "ruleDefinicaoColunas"
    // InternalPlan2Cand.g:180:1: ruleDefinicaoColunas returns [EObject current=null] : (otherlv_0= 'coluna' ( (lv_coluna_1_0= RULE_INT ) ) otherlv_2= ':' otherlv_3= 'destino' otherlv_4= '=' ( (lv_destino_5_0= RULE_COLUNA_BANCO ) ) ( (lv_transformador_6_0= ruleTransformador ) )? otherlv_7= ';' ) ;
    public final EObject ruleDefinicaoColunas() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_coluna_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_destino_5_0=null;
        Token otherlv_7=null;
        EObject lv_transformador_6_0 = null;



        	enterRule();

        try {
            // InternalPlan2Cand.g:186:2: ( (otherlv_0= 'coluna' ( (lv_coluna_1_0= RULE_INT ) ) otherlv_2= ':' otherlv_3= 'destino' otherlv_4= '=' ( (lv_destino_5_0= RULE_COLUNA_BANCO ) ) ( (lv_transformador_6_0= ruleTransformador ) )? otherlv_7= ';' ) )
            // InternalPlan2Cand.g:187:2: (otherlv_0= 'coluna' ( (lv_coluna_1_0= RULE_INT ) ) otherlv_2= ':' otherlv_3= 'destino' otherlv_4= '=' ( (lv_destino_5_0= RULE_COLUNA_BANCO ) ) ( (lv_transformador_6_0= ruleTransformador ) )? otherlv_7= ';' )
            {
            // InternalPlan2Cand.g:187:2: (otherlv_0= 'coluna' ( (lv_coluna_1_0= RULE_INT ) ) otherlv_2= ':' otherlv_3= 'destino' otherlv_4= '=' ( (lv_destino_5_0= RULE_COLUNA_BANCO ) ) ( (lv_transformador_6_0= ruleTransformador ) )? otherlv_7= ';' )
            // InternalPlan2Cand.g:188:3: otherlv_0= 'coluna' ( (lv_coluna_1_0= RULE_INT ) ) otherlv_2= ':' otherlv_3= 'destino' otherlv_4= '=' ( (lv_destino_5_0= RULE_COLUNA_BANCO ) ) ( (lv_transformador_6_0= ruleTransformador ) )? otherlv_7= ';'
            {
            otherlv_0=(Token)match(input,20,FOLLOW_9); 

            			newLeafNode(otherlv_0, grammarAccess.getDefinicaoColunasAccess().getColunaKeyword_0());
            		
            // InternalPlan2Cand.g:192:3: ( (lv_coluna_1_0= RULE_INT ) )
            // InternalPlan2Cand.g:193:4: (lv_coluna_1_0= RULE_INT )
            {
            // InternalPlan2Cand.g:193:4: (lv_coluna_1_0= RULE_INT )
            // InternalPlan2Cand.g:194:5: lv_coluna_1_0= RULE_INT
            {
            lv_coluna_1_0=(Token)match(input,RULE_INT,FOLLOW_10); 

            					newLeafNode(lv_coluna_1_0, grammarAccess.getDefinicaoColunasAccess().getColunaINTTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDefinicaoColunasRule());
            					}
            					setWithLastConsumed(
            						current,
            						"coluna",
            						lv_coluna_1_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }

            otherlv_2=(Token)match(input,21,FOLLOW_11); 

            			newLeafNode(otherlv_2, grammarAccess.getDefinicaoColunasAccess().getColonKeyword_2());
            		
            otherlv_3=(Token)match(input,22,FOLLOW_12); 

            			newLeafNode(otherlv_3, grammarAccess.getDefinicaoColunasAccess().getDestinoKeyword_3());
            		
            otherlv_4=(Token)match(input,23,FOLLOW_13); 

            			newLeafNode(otherlv_4, grammarAccess.getDefinicaoColunasAccess().getEqualsSignKeyword_4());
            		
            // InternalPlan2Cand.g:222:3: ( (lv_destino_5_0= RULE_COLUNA_BANCO ) )
            // InternalPlan2Cand.g:223:4: (lv_destino_5_0= RULE_COLUNA_BANCO )
            {
            // InternalPlan2Cand.g:223:4: (lv_destino_5_0= RULE_COLUNA_BANCO )
            // InternalPlan2Cand.g:224:5: lv_destino_5_0= RULE_COLUNA_BANCO
            {
            lv_destino_5_0=(Token)match(input,RULE_COLUNA_BANCO,FOLLOW_14); 

            					newLeafNode(lv_destino_5_0, grammarAccess.getDefinicaoColunasAccess().getDestinoCOLUNA_BANCOTerminalRuleCall_5_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDefinicaoColunasRule());
            					}
            					setWithLastConsumed(
            						current,
            						"destino",
            						lv_destino_5_0,
            						"br.comperve.xtext.candidato_import.Plan2Cand.COLUNA_BANCO");
            				

            }


            }

            // InternalPlan2Cand.g:240:3: ( (lv_transformador_6_0= ruleTransformador ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==25) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalPlan2Cand.g:241:4: (lv_transformador_6_0= ruleTransformador )
                    {
                    // InternalPlan2Cand.g:241:4: (lv_transformador_6_0= ruleTransformador )
                    // InternalPlan2Cand.g:242:5: lv_transformador_6_0= ruleTransformador
                    {

                    					newCompositeNode(grammarAccess.getDefinicaoColunasAccess().getTransformadorTransformadorParserRuleCall_6_0());
                    				
                    pushFollow(FOLLOW_15);
                    lv_transformador_6_0=ruleTransformador();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDefinicaoColunasRule());
                    					}
                    					set(
                    						current,
                    						"transformador",
                    						lv_transformador_6_0,
                    						"br.comperve.xtext.candidato_import.Plan2Cand.Transformador");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getDefinicaoColunasAccess().getSemicolonKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefinicaoColunas"


    // $ANTLR start "entryRuleTransformador"
    // InternalPlan2Cand.g:267:1: entryRuleTransformador returns [EObject current=null] : iv_ruleTransformador= ruleTransformador EOF ;
    public final EObject entryRuleTransformador() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransformador = null;


        try {
            // InternalPlan2Cand.g:267:54: (iv_ruleTransformador= ruleTransformador EOF )
            // InternalPlan2Cand.g:268:2: iv_ruleTransformador= ruleTransformador EOF
            {
             newCompositeNode(grammarAccess.getTransformadorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransformador=ruleTransformador();

            state._fsp--;

             current =iv_ruleTransformador; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransformador"


    // $ANTLR start "ruleTransformador"
    // InternalPlan2Cand.g:274:1: ruleTransformador returns [EObject current=null] : (otherlv_0= ',' ( ( (lv_maiuscula_1_0= 'maiusculo' ) ) | ( (lv_minuscula_2_0= 'minusculo' ) ) | ( (lv_apenasNumeros_3_0= 'apenas_numeros' ) ) ) ) ;
    public final EObject ruleTransformador() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_maiuscula_1_0=null;
        Token lv_minuscula_2_0=null;
        Token lv_apenasNumeros_3_0=null;


        	enterRule();

        try {
            // InternalPlan2Cand.g:280:2: ( (otherlv_0= ',' ( ( (lv_maiuscula_1_0= 'maiusculo' ) ) | ( (lv_minuscula_2_0= 'minusculo' ) ) | ( (lv_apenasNumeros_3_0= 'apenas_numeros' ) ) ) ) )
            // InternalPlan2Cand.g:281:2: (otherlv_0= ',' ( ( (lv_maiuscula_1_0= 'maiusculo' ) ) | ( (lv_minuscula_2_0= 'minusculo' ) ) | ( (lv_apenasNumeros_3_0= 'apenas_numeros' ) ) ) )
            {
            // InternalPlan2Cand.g:281:2: (otherlv_0= ',' ( ( (lv_maiuscula_1_0= 'maiusculo' ) ) | ( (lv_minuscula_2_0= 'minusculo' ) ) | ( (lv_apenasNumeros_3_0= 'apenas_numeros' ) ) ) )
            // InternalPlan2Cand.g:282:3: otherlv_0= ',' ( ( (lv_maiuscula_1_0= 'maiusculo' ) ) | ( (lv_minuscula_2_0= 'minusculo' ) ) | ( (lv_apenasNumeros_3_0= 'apenas_numeros' ) ) )
            {
            otherlv_0=(Token)match(input,25,FOLLOW_16); 

            			newLeafNode(otherlv_0, grammarAccess.getTransformadorAccess().getCommaKeyword_0());
            		
            // InternalPlan2Cand.g:286:3: ( ( (lv_maiuscula_1_0= 'maiusculo' ) ) | ( (lv_minuscula_2_0= 'minusculo' ) ) | ( (lv_apenasNumeros_3_0= 'apenas_numeros' ) ) )
            int alt4=3;
            switch ( input.LA(1) ) {
            case 26:
                {
                alt4=1;
                }
                break;
            case 27:
                {
                alt4=2;
                }
                break;
            case 28:
                {
                alt4=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalPlan2Cand.g:287:4: ( (lv_maiuscula_1_0= 'maiusculo' ) )
                    {
                    // InternalPlan2Cand.g:287:4: ( (lv_maiuscula_1_0= 'maiusculo' ) )
                    // InternalPlan2Cand.g:288:5: (lv_maiuscula_1_0= 'maiusculo' )
                    {
                    // InternalPlan2Cand.g:288:5: (lv_maiuscula_1_0= 'maiusculo' )
                    // InternalPlan2Cand.g:289:6: lv_maiuscula_1_0= 'maiusculo'
                    {
                    lv_maiuscula_1_0=(Token)match(input,26,FOLLOW_2); 

                    						newLeafNode(lv_maiuscula_1_0, grammarAccess.getTransformadorAccess().getMaiusculaMaiusculoKeyword_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTransformadorRule());
                    						}
                    						setWithLastConsumed(current, "maiuscula", lv_maiuscula_1_0 != null, "maiusculo");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPlan2Cand.g:302:4: ( (lv_minuscula_2_0= 'minusculo' ) )
                    {
                    // InternalPlan2Cand.g:302:4: ( (lv_minuscula_2_0= 'minusculo' ) )
                    // InternalPlan2Cand.g:303:5: (lv_minuscula_2_0= 'minusculo' )
                    {
                    // InternalPlan2Cand.g:303:5: (lv_minuscula_2_0= 'minusculo' )
                    // InternalPlan2Cand.g:304:6: lv_minuscula_2_0= 'minusculo'
                    {
                    lv_minuscula_2_0=(Token)match(input,27,FOLLOW_2); 

                    						newLeafNode(lv_minuscula_2_0, grammarAccess.getTransformadorAccess().getMinusculaMinusculoKeyword_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTransformadorRule());
                    						}
                    						setWithLastConsumed(current, "minuscula", lv_minuscula_2_0 != null, "minusculo");
                    					

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalPlan2Cand.g:317:4: ( (lv_apenasNumeros_3_0= 'apenas_numeros' ) )
                    {
                    // InternalPlan2Cand.g:317:4: ( (lv_apenasNumeros_3_0= 'apenas_numeros' ) )
                    // InternalPlan2Cand.g:318:5: (lv_apenasNumeros_3_0= 'apenas_numeros' )
                    {
                    // InternalPlan2Cand.g:318:5: (lv_apenasNumeros_3_0= 'apenas_numeros' )
                    // InternalPlan2Cand.g:319:6: lv_apenasNumeros_3_0= 'apenas_numeros'
                    {
                    lv_apenasNumeros_3_0=(Token)match(input,28,FOLLOW_2); 

                    						newLeafNode(lv_apenasNumeros_3_0, grammarAccess.getTransformadorAccess().getApenasNumerosApenas_numerosKeyword_1_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTransformadorRule());
                    						}
                    						setWithLastConsumed(current, "apenasNumeros", lv_apenasNumeros_3_0 != null, "apenas_numeros");
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransformador"


    // $ANTLR start "entryRuleLinhaDados"
    // InternalPlan2Cand.g:336:1: entryRuleLinhaDados returns [EObject current=null] : iv_ruleLinhaDados= ruleLinhaDados EOF ;
    public final EObject entryRuleLinhaDados() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLinhaDados = null;


        try {
            // InternalPlan2Cand.g:336:51: (iv_ruleLinhaDados= ruleLinhaDados EOF )
            // InternalPlan2Cand.g:337:2: iv_ruleLinhaDados= ruleLinhaDados EOF
            {
             newCompositeNode(grammarAccess.getLinhaDadosRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLinhaDados=ruleLinhaDados();

            state._fsp--;

             current =iv_ruleLinhaDados; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLinhaDados"


    // $ANTLR start "ruleLinhaDados"
    // InternalPlan2Cand.g:343:1: ruleLinhaDados returns [EObject current=null] : ( ( (lv_linha_0_0= ruleTipoDado ) ) (this_SEPARADOR_1= RULE_SEPARADOR ( (lv_linha_2_0= ruleTipoDado ) ) )* ) ;
    public final EObject ruleLinhaDados() throws RecognitionException {
        EObject current = null;

        Token this_SEPARADOR_1=null;
        AntlrDatatypeRuleToken lv_linha_0_0 = null;

        AntlrDatatypeRuleToken lv_linha_2_0 = null;



        	enterRule();

        try {
            // InternalPlan2Cand.g:349:2: ( ( ( (lv_linha_0_0= ruleTipoDado ) ) (this_SEPARADOR_1= RULE_SEPARADOR ( (lv_linha_2_0= ruleTipoDado ) ) )* ) )
            // InternalPlan2Cand.g:350:2: ( ( (lv_linha_0_0= ruleTipoDado ) ) (this_SEPARADOR_1= RULE_SEPARADOR ( (lv_linha_2_0= ruleTipoDado ) ) )* )
            {
            // InternalPlan2Cand.g:350:2: ( ( (lv_linha_0_0= ruleTipoDado ) ) (this_SEPARADOR_1= RULE_SEPARADOR ( (lv_linha_2_0= ruleTipoDado ) ) )* )
            // InternalPlan2Cand.g:351:3: ( (lv_linha_0_0= ruleTipoDado ) ) (this_SEPARADOR_1= RULE_SEPARADOR ( (lv_linha_2_0= ruleTipoDado ) ) )*
            {
            // InternalPlan2Cand.g:351:3: ( (lv_linha_0_0= ruleTipoDado ) )
            // InternalPlan2Cand.g:352:4: (lv_linha_0_0= ruleTipoDado )
            {
            // InternalPlan2Cand.g:352:4: (lv_linha_0_0= ruleTipoDado )
            // InternalPlan2Cand.g:353:5: lv_linha_0_0= ruleTipoDado
            {

            					newCompositeNode(grammarAccess.getLinhaDadosAccess().getLinhaTipoDadoParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_17);
            lv_linha_0_0=ruleTipoDado();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLinhaDadosRule());
            					}
            					add(
            						current,
            						"linha",
            						lv_linha_0_0,
            						"br.comperve.xtext.candidato_import.Plan2Cand.TipoDado");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPlan2Cand.g:370:3: (this_SEPARADOR_1= RULE_SEPARADOR ( (lv_linha_2_0= ruleTipoDado ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_SEPARADOR) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalPlan2Cand.g:371:4: this_SEPARADOR_1= RULE_SEPARADOR ( (lv_linha_2_0= ruleTipoDado ) )
            	    {
            	    this_SEPARADOR_1=(Token)match(input,RULE_SEPARADOR,FOLLOW_6); 

            	    				newLeafNode(this_SEPARADOR_1, grammarAccess.getLinhaDadosAccess().getSEPARADORTerminalRuleCall_1_0());
            	    			
            	    // InternalPlan2Cand.g:375:4: ( (lv_linha_2_0= ruleTipoDado ) )
            	    // InternalPlan2Cand.g:376:5: (lv_linha_2_0= ruleTipoDado )
            	    {
            	    // InternalPlan2Cand.g:376:5: (lv_linha_2_0= ruleTipoDado )
            	    // InternalPlan2Cand.g:377:6: lv_linha_2_0= ruleTipoDado
            	    {

            	    						newCompositeNode(grammarAccess.getLinhaDadosAccess().getLinhaTipoDadoParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_17);
            	    lv_linha_2_0=ruleTipoDado();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getLinhaDadosRule());
            	    						}
            	    						add(
            	    							current,
            	    							"linha",
            	    							lv_linha_2_0,
            	    							"br.comperve.xtext.candidato_import.Plan2Cand.TipoDado");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLinhaDados"


    // $ANTLR start "entryRuleTipoDado"
    // InternalPlan2Cand.g:399:1: entryRuleTipoDado returns [String current=null] : iv_ruleTipoDado= ruleTipoDado EOF ;
    public final String entryRuleTipoDado() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleTipoDado = null;


        try {
            // InternalPlan2Cand.g:399:48: (iv_ruleTipoDado= ruleTipoDado EOF )
            // InternalPlan2Cand.g:400:2: iv_ruleTipoDado= ruleTipoDado EOF
            {
             newCompositeNode(grammarAccess.getTipoDadoRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTipoDado=ruleTipoDado();

            state._fsp--;

             current =iv_ruleTipoDado.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTipoDado"


    // $ANTLR start "ruleTipoDado"
    // InternalPlan2Cand.g:406:1: ruleTipoDado returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT | this_CPF_FORMATADO_1= RULE_CPF_FORMATADO | this_EMAIL_2= RULE_EMAIL | this_NOME_3= RULE_NOME | this_ID_4= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleTipoDado() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token this_CPF_FORMATADO_1=null;
        Token this_EMAIL_2=null;
        Token this_NOME_3=null;
        Token this_ID_4=null;


        	enterRule();

        try {
            // InternalPlan2Cand.g:412:2: ( (this_INT_0= RULE_INT | this_CPF_FORMATADO_1= RULE_CPF_FORMATADO | this_EMAIL_2= RULE_EMAIL | this_NOME_3= RULE_NOME | this_ID_4= RULE_ID ) )
            // InternalPlan2Cand.g:413:2: (this_INT_0= RULE_INT | this_CPF_FORMATADO_1= RULE_CPF_FORMATADO | this_EMAIL_2= RULE_EMAIL | this_NOME_3= RULE_NOME | this_ID_4= RULE_ID )
            {
            // InternalPlan2Cand.g:413:2: (this_INT_0= RULE_INT | this_CPF_FORMATADO_1= RULE_CPF_FORMATADO | this_EMAIL_2= RULE_EMAIL | this_NOME_3= RULE_NOME | this_ID_4= RULE_ID )
            int alt6=5;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                alt6=1;
                }
                break;
            case RULE_CPF_FORMATADO:
                {
                alt6=2;
                }
                break;
            case RULE_EMAIL:
                {
                alt6=3;
                }
                break;
            case RULE_NOME:
                {
                alt6=4;
                }
                break;
            case RULE_ID:
                {
                alt6=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalPlan2Cand.g:414:3: this_INT_0= RULE_INT
                    {
                    this_INT_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    			current.merge(this_INT_0);
                    		

                    			newLeafNode(this_INT_0, grammarAccess.getTipoDadoAccess().getINTTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalPlan2Cand.g:422:3: this_CPF_FORMATADO_1= RULE_CPF_FORMATADO
                    {
                    this_CPF_FORMATADO_1=(Token)match(input,RULE_CPF_FORMATADO,FOLLOW_2); 

                    			current.merge(this_CPF_FORMATADO_1);
                    		

                    			newLeafNode(this_CPF_FORMATADO_1, grammarAccess.getTipoDadoAccess().getCPF_FORMATADOTerminalRuleCall_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalPlan2Cand.g:430:3: this_EMAIL_2= RULE_EMAIL
                    {
                    this_EMAIL_2=(Token)match(input,RULE_EMAIL,FOLLOW_2); 

                    			current.merge(this_EMAIL_2);
                    		

                    			newLeafNode(this_EMAIL_2, grammarAccess.getTipoDadoAccess().getEMAILTerminalRuleCall_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalPlan2Cand.g:438:3: this_NOME_3= RULE_NOME
                    {
                    this_NOME_3=(Token)match(input,RULE_NOME,FOLLOW_2); 

                    			current.merge(this_NOME_3);
                    		

                    			newLeafNode(this_NOME_3, grammarAccess.getTipoDadoAccess().getNOMETerminalRuleCall_3());
                    		

                    }
                    break;
                case 5 :
                    // InternalPlan2Cand.g:446:3: this_ID_4= RULE_ID
                    {
                    this_ID_4=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_4);
                    		

                    			newLeafNode(this_ID_4, grammarAccess.getTipoDadoAccess().getIDTerminalRuleCall_4());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTipoDado"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000120000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000790L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000792L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000003000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000001C000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000042L});

}