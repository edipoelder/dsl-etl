/**
 * generated by Xtext 2.25.0
 */
package br.comperve.xtext.candidato_import.plan2Cand.impl;

import br.comperve.xtext.candidato_import.plan2Cand.DefinicaoColunas;
import br.comperve.xtext.candidato_import.plan2Cand.Plan2CandPackage;
import br.comperve.xtext.candidato_import.plan2Cand.Transformador;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Definicao Colunas</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.comperve.xtext.candidato_import.plan2Cand.impl.DefinicaoColunasImpl#getColuna <em>Coluna</em>}</li>
 *   <li>{@link br.comperve.xtext.candidato_import.plan2Cand.impl.DefinicaoColunasImpl#getDestino <em>Destino</em>}</li>
 *   <li>{@link br.comperve.xtext.candidato_import.plan2Cand.impl.DefinicaoColunasImpl#getTransformador <em>Transformador</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DefinicaoColunasImpl extends MinimalEObjectImpl.Container implements DefinicaoColunas
{
  /**
   * The default value of the '{@link #getColuna() <em>Coluna</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getColuna()
   * @generated
   * @ordered
   */
  protected static final int COLUNA_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getColuna() <em>Coluna</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getColuna()
   * @generated
   * @ordered
   */
  protected int coluna = COLUNA_EDEFAULT;

  /**
   * The default value of the '{@link #getDestino() <em>Destino</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDestino()
   * @generated
   * @ordered
   */
  protected static final String DESTINO_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDestino() <em>Destino</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDestino()
   * @generated
   * @ordered
   */
  protected String destino = DESTINO_EDEFAULT;

  /**
   * The cached value of the '{@link #getTransformador() <em>Transformador</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTransformador()
   * @generated
   * @ordered
   */
  protected Transformador transformador;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DefinicaoColunasImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return Plan2CandPackage.Literals.DEFINICAO_COLUNAS;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int getColuna()
  {
    return coluna;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setColuna(int newColuna)
  {
    int oldColuna = coluna;
    coluna = newColuna;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, Plan2CandPackage.DEFINICAO_COLUNAS__COLUNA, oldColuna, coluna));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getDestino()
  {
    return destino;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setDestino(String newDestino)
  {
    String oldDestino = destino;
    destino = newDestino;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, Plan2CandPackage.DEFINICAO_COLUNAS__DESTINO, oldDestino, destino));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Transformador getTransformador()
  {
    return transformador;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTransformador(Transformador newTransformador, NotificationChain msgs)
  {
    Transformador oldTransformador = transformador;
    transformador = newTransformador;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Plan2CandPackage.DEFINICAO_COLUNAS__TRANSFORMADOR, oldTransformador, newTransformador);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setTransformador(Transformador newTransformador)
  {
    if (newTransformador != transformador)
    {
      NotificationChain msgs = null;
      if (transformador != null)
        msgs = ((InternalEObject)transformador).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Plan2CandPackage.DEFINICAO_COLUNAS__TRANSFORMADOR, null, msgs);
      if (newTransformador != null)
        msgs = ((InternalEObject)newTransformador).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Plan2CandPackage.DEFINICAO_COLUNAS__TRANSFORMADOR, null, msgs);
      msgs = basicSetTransformador(newTransformador, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, Plan2CandPackage.DEFINICAO_COLUNAS__TRANSFORMADOR, newTransformador, newTransformador));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case Plan2CandPackage.DEFINICAO_COLUNAS__TRANSFORMADOR:
        return basicSetTransformador(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case Plan2CandPackage.DEFINICAO_COLUNAS__COLUNA:
        return getColuna();
      case Plan2CandPackage.DEFINICAO_COLUNAS__DESTINO:
        return getDestino();
      case Plan2CandPackage.DEFINICAO_COLUNAS__TRANSFORMADOR:
        return getTransformador();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case Plan2CandPackage.DEFINICAO_COLUNAS__COLUNA:
        setColuna((Integer)newValue);
        return;
      case Plan2CandPackage.DEFINICAO_COLUNAS__DESTINO:
        setDestino((String)newValue);
        return;
      case Plan2CandPackage.DEFINICAO_COLUNAS__TRANSFORMADOR:
        setTransformador((Transformador)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case Plan2CandPackage.DEFINICAO_COLUNAS__COLUNA:
        setColuna(COLUNA_EDEFAULT);
        return;
      case Plan2CandPackage.DEFINICAO_COLUNAS__DESTINO:
        setDestino(DESTINO_EDEFAULT);
        return;
      case Plan2CandPackage.DEFINICAO_COLUNAS__TRANSFORMADOR:
        setTransformador((Transformador)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case Plan2CandPackage.DEFINICAO_COLUNAS__COLUNA:
        return coluna != COLUNA_EDEFAULT;
      case Plan2CandPackage.DEFINICAO_COLUNAS__DESTINO:
        return DESTINO_EDEFAULT == null ? destino != null : !DESTINO_EDEFAULT.equals(destino);
      case Plan2CandPackage.DEFINICAO_COLUNAS__TRANSFORMADOR:
        return transformador != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (coluna: ");
    result.append(coluna);
    result.append(", destino: ");
    result.append(destino);
    result.append(')');
    return result.toString();
  }

} //DefinicaoColunasImpl
