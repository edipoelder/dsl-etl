/**
 * generated by Xtext 2.25.0
 */
package br.comperve.xtext.candidato_import.plan2Cand;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Linha Dados</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.comperve.xtext.candidato_import.plan2Cand.LinhaDados#getLinha <em>Linha</em>}</li>
 * </ul>
 *
 * @see br.comperve.xtext.candidato_import.plan2Cand.Plan2CandPackage#getLinhaDados()
 * @model
 * @generated
 */
public interface LinhaDados extends EObject
{
  /**
   * Returns the value of the '<em><b>Linha</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Linha</em>' attribute list.
   * @see br.comperve.xtext.candidato_import.plan2Cand.Plan2CandPackage#getLinhaDados_Linha()
   * @model unique="false"
   * @generated
   */
  EList<String> getLinha();

} // LinhaDados
