/*
 * generated by Xtext 2.25.0
 */
package br.comperve.xtext.candidato_import.ide;

import br.comperve.xtext.candidato_import.Plan2CandRuntimeModule;
import br.comperve.xtext.candidato_import.Plan2CandStandaloneSetup;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
public class Plan2CandIdeSetup extends Plan2CandStandaloneSetup {

	@Override
	public Injector createInjector() {
		return Guice.createInjector(Modules2.mixin(new Plan2CandRuntimeModule(), new Plan2CandIdeModule()));
	}
	
}
