package br.comperve.xtext.candidato_import.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import br.comperve.xtext.candidato_import.services.Plan2CandGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPlan2CandParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_CPF_FORMATADO", "RULE_EMAIL", "RULE_NOME", "RULE_ID", "RULE_SEPARADOR", "RULE_COLUNA_BANCO", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'BEGIN_MAPEAMENTO'", "'END_MAPEAMENTO'", "'BEGIN_DADOS'", "'END_DADOS'", "'coluna'", "':'", "'destino'", "'='", "';'", "','", "'maiusculo'", "'minusculo'", "'apenas_numeros'"
    };
    public static final int RULE_COLUNA_BANCO=10;
    public static final int RULE_EMAIL=6;
    public static final int RULE_STRING=11;
    public static final int RULE_SL_COMMENT=13;
    public static final int T__19=19;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int RULE_NOME=7;
    public static final int EOF=-1;
    public static final int RULE_ID=8;
    public static final int RULE_WS=14;
    public static final int RULE_CPF_FORMATADO=5;
    public static final int RULE_ANY_OTHER=15;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=12;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int RULE_SEPARADOR=9;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalPlan2CandParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPlan2CandParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPlan2CandParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPlan2Cand.g"; }


    	private Plan2CandGrammarAccess grammarAccess;

    	public void setGrammarAccess(Plan2CandGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleModel"
    // InternalPlan2Cand.g:53:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // InternalPlan2Cand.g:54:1: ( ruleModel EOF )
            // InternalPlan2Cand.g:55:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalPlan2Cand.g:62:1: ruleModel : ( ( rule__Model__Group__0 ) ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:66:2: ( ( ( rule__Model__Group__0 ) ) )
            // InternalPlan2Cand.g:67:2: ( ( rule__Model__Group__0 ) )
            {
            // InternalPlan2Cand.g:67:2: ( ( rule__Model__Group__0 ) )
            // InternalPlan2Cand.g:68:3: ( rule__Model__Group__0 )
            {
             before(grammarAccess.getModelAccess().getGroup()); 
            // InternalPlan2Cand.g:69:3: ( rule__Model__Group__0 )
            // InternalPlan2Cand.g:69:4: rule__Model__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleDados"
    // InternalPlan2Cand.g:78:1: entryRuleDados : ruleDados EOF ;
    public final void entryRuleDados() throws RecognitionException {
        try {
            // InternalPlan2Cand.g:79:1: ( ruleDados EOF )
            // InternalPlan2Cand.g:80:1: ruleDados EOF
            {
             before(grammarAccess.getDadosRule()); 
            pushFollow(FOLLOW_1);
            ruleDados();

            state._fsp--;

             after(grammarAccess.getDadosRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDados"


    // $ANTLR start "ruleDados"
    // InternalPlan2Cand.g:87:1: ruleDados : ( ( ( rule__Dados__DadosAssignment ) ) ( ( rule__Dados__DadosAssignment )* ) ) ;
    public final void ruleDados() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:91:2: ( ( ( ( rule__Dados__DadosAssignment ) ) ( ( rule__Dados__DadosAssignment )* ) ) )
            // InternalPlan2Cand.g:92:2: ( ( ( rule__Dados__DadosAssignment ) ) ( ( rule__Dados__DadosAssignment )* ) )
            {
            // InternalPlan2Cand.g:92:2: ( ( ( rule__Dados__DadosAssignment ) ) ( ( rule__Dados__DadosAssignment )* ) )
            // InternalPlan2Cand.g:93:3: ( ( rule__Dados__DadosAssignment ) ) ( ( rule__Dados__DadosAssignment )* )
            {
            // InternalPlan2Cand.g:93:3: ( ( rule__Dados__DadosAssignment ) )
            // InternalPlan2Cand.g:94:4: ( rule__Dados__DadosAssignment )
            {
             before(grammarAccess.getDadosAccess().getDadosAssignment()); 
            // InternalPlan2Cand.g:95:4: ( rule__Dados__DadosAssignment )
            // InternalPlan2Cand.g:95:5: rule__Dados__DadosAssignment
            {
            pushFollow(FOLLOW_3);
            rule__Dados__DadosAssignment();

            state._fsp--;


            }

             after(grammarAccess.getDadosAccess().getDadosAssignment()); 

            }

            // InternalPlan2Cand.g:98:3: ( ( rule__Dados__DadosAssignment )* )
            // InternalPlan2Cand.g:99:4: ( rule__Dados__DadosAssignment )*
            {
             before(grammarAccess.getDadosAccess().getDadosAssignment()); 
            // InternalPlan2Cand.g:100:4: ( rule__Dados__DadosAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_INT && LA1_0<=RULE_ID)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPlan2Cand.g:100:5: rule__Dados__DadosAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Dados__DadosAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getDadosAccess().getDadosAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDados"


    // $ANTLR start "entryRuleDefinicaoColunas"
    // InternalPlan2Cand.g:110:1: entryRuleDefinicaoColunas : ruleDefinicaoColunas EOF ;
    public final void entryRuleDefinicaoColunas() throws RecognitionException {
        try {
            // InternalPlan2Cand.g:111:1: ( ruleDefinicaoColunas EOF )
            // InternalPlan2Cand.g:112:1: ruleDefinicaoColunas EOF
            {
             before(grammarAccess.getDefinicaoColunasRule()); 
            pushFollow(FOLLOW_1);
            ruleDefinicaoColunas();

            state._fsp--;

             after(grammarAccess.getDefinicaoColunasRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDefinicaoColunas"


    // $ANTLR start "ruleDefinicaoColunas"
    // InternalPlan2Cand.g:119:1: ruleDefinicaoColunas : ( ( rule__DefinicaoColunas__Group__0 ) ) ;
    public final void ruleDefinicaoColunas() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:123:2: ( ( ( rule__DefinicaoColunas__Group__0 ) ) )
            // InternalPlan2Cand.g:124:2: ( ( rule__DefinicaoColunas__Group__0 ) )
            {
            // InternalPlan2Cand.g:124:2: ( ( rule__DefinicaoColunas__Group__0 ) )
            // InternalPlan2Cand.g:125:3: ( rule__DefinicaoColunas__Group__0 )
            {
             before(grammarAccess.getDefinicaoColunasAccess().getGroup()); 
            // InternalPlan2Cand.g:126:3: ( rule__DefinicaoColunas__Group__0 )
            // InternalPlan2Cand.g:126:4: rule__DefinicaoColunas__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DefinicaoColunas__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDefinicaoColunasAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDefinicaoColunas"


    // $ANTLR start "entryRuleTransformador"
    // InternalPlan2Cand.g:135:1: entryRuleTransformador : ruleTransformador EOF ;
    public final void entryRuleTransformador() throws RecognitionException {
        try {
            // InternalPlan2Cand.g:136:1: ( ruleTransformador EOF )
            // InternalPlan2Cand.g:137:1: ruleTransformador EOF
            {
             before(grammarAccess.getTransformadorRule()); 
            pushFollow(FOLLOW_1);
            ruleTransformador();

            state._fsp--;

             after(grammarAccess.getTransformadorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTransformador"


    // $ANTLR start "ruleTransformador"
    // InternalPlan2Cand.g:144:1: ruleTransformador : ( ( rule__Transformador__Group__0 ) ) ;
    public final void ruleTransformador() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:148:2: ( ( ( rule__Transformador__Group__0 ) ) )
            // InternalPlan2Cand.g:149:2: ( ( rule__Transformador__Group__0 ) )
            {
            // InternalPlan2Cand.g:149:2: ( ( rule__Transformador__Group__0 ) )
            // InternalPlan2Cand.g:150:3: ( rule__Transformador__Group__0 )
            {
             before(grammarAccess.getTransformadorAccess().getGroup()); 
            // InternalPlan2Cand.g:151:3: ( rule__Transformador__Group__0 )
            // InternalPlan2Cand.g:151:4: rule__Transformador__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Transformador__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTransformadorAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTransformador"


    // $ANTLR start "entryRuleLinhaDados"
    // InternalPlan2Cand.g:160:1: entryRuleLinhaDados : ruleLinhaDados EOF ;
    public final void entryRuleLinhaDados() throws RecognitionException {
        try {
            // InternalPlan2Cand.g:161:1: ( ruleLinhaDados EOF )
            // InternalPlan2Cand.g:162:1: ruleLinhaDados EOF
            {
             before(grammarAccess.getLinhaDadosRule()); 
            pushFollow(FOLLOW_1);
            ruleLinhaDados();

            state._fsp--;

             after(grammarAccess.getLinhaDadosRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLinhaDados"


    // $ANTLR start "ruleLinhaDados"
    // InternalPlan2Cand.g:169:1: ruleLinhaDados : ( ( rule__LinhaDados__Group__0 ) ) ;
    public final void ruleLinhaDados() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:173:2: ( ( ( rule__LinhaDados__Group__0 ) ) )
            // InternalPlan2Cand.g:174:2: ( ( rule__LinhaDados__Group__0 ) )
            {
            // InternalPlan2Cand.g:174:2: ( ( rule__LinhaDados__Group__0 ) )
            // InternalPlan2Cand.g:175:3: ( rule__LinhaDados__Group__0 )
            {
             before(grammarAccess.getLinhaDadosAccess().getGroup()); 
            // InternalPlan2Cand.g:176:3: ( rule__LinhaDados__Group__0 )
            // InternalPlan2Cand.g:176:4: rule__LinhaDados__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LinhaDados__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLinhaDadosAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLinhaDados"


    // $ANTLR start "entryRuleTipoDado"
    // InternalPlan2Cand.g:185:1: entryRuleTipoDado : ruleTipoDado EOF ;
    public final void entryRuleTipoDado() throws RecognitionException {
        try {
            // InternalPlan2Cand.g:186:1: ( ruleTipoDado EOF )
            // InternalPlan2Cand.g:187:1: ruleTipoDado EOF
            {
             before(grammarAccess.getTipoDadoRule()); 
            pushFollow(FOLLOW_1);
            ruleTipoDado();

            state._fsp--;

             after(grammarAccess.getTipoDadoRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTipoDado"


    // $ANTLR start "ruleTipoDado"
    // InternalPlan2Cand.g:194:1: ruleTipoDado : ( ( rule__TipoDado__Alternatives ) ) ;
    public final void ruleTipoDado() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:198:2: ( ( ( rule__TipoDado__Alternatives ) ) )
            // InternalPlan2Cand.g:199:2: ( ( rule__TipoDado__Alternatives ) )
            {
            // InternalPlan2Cand.g:199:2: ( ( rule__TipoDado__Alternatives ) )
            // InternalPlan2Cand.g:200:3: ( rule__TipoDado__Alternatives )
            {
             before(grammarAccess.getTipoDadoAccess().getAlternatives()); 
            // InternalPlan2Cand.g:201:3: ( rule__TipoDado__Alternatives )
            // InternalPlan2Cand.g:201:4: rule__TipoDado__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TipoDado__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTipoDadoAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTipoDado"


    // $ANTLR start "rule__Transformador__Alternatives_1"
    // InternalPlan2Cand.g:209:1: rule__Transformador__Alternatives_1 : ( ( ( rule__Transformador__MaiusculaAssignment_1_0 ) ) | ( ( rule__Transformador__MinusculaAssignment_1_1 ) ) | ( ( rule__Transformador__ApenasNumerosAssignment_1_2 ) ) );
    public final void rule__Transformador__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:213:1: ( ( ( rule__Transformador__MaiusculaAssignment_1_0 ) ) | ( ( rule__Transformador__MinusculaAssignment_1_1 ) ) | ( ( rule__Transformador__ApenasNumerosAssignment_1_2 ) ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 26:
                {
                alt2=1;
                }
                break;
            case 27:
                {
                alt2=2;
                }
                break;
            case 28:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalPlan2Cand.g:214:2: ( ( rule__Transformador__MaiusculaAssignment_1_0 ) )
                    {
                    // InternalPlan2Cand.g:214:2: ( ( rule__Transformador__MaiusculaAssignment_1_0 ) )
                    // InternalPlan2Cand.g:215:3: ( rule__Transformador__MaiusculaAssignment_1_0 )
                    {
                     before(grammarAccess.getTransformadorAccess().getMaiusculaAssignment_1_0()); 
                    // InternalPlan2Cand.g:216:3: ( rule__Transformador__MaiusculaAssignment_1_0 )
                    // InternalPlan2Cand.g:216:4: rule__Transformador__MaiusculaAssignment_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Transformador__MaiusculaAssignment_1_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getTransformadorAccess().getMaiusculaAssignment_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPlan2Cand.g:220:2: ( ( rule__Transformador__MinusculaAssignment_1_1 ) )
                    {
                    // InternalPlan2Cand.g:220:2: ( ( rule__Transformador__MinusculaAssignment_1_1 ) )
                    // InternalPlan2Cand.g:221:3: ( rule__Transformador__MinusculaAssignment_1_1 )
                    {
                     before(grammarAccess.getTransformadorAccess().getMinusculaAssignment_1_1()); 
                    // InternalPlan2Cand.g:222:3: ( rule__Transformador__MinusculaAssignment_1_1 )
                    // InternalPlan2Cand.g:222:4: rule__Transformador__MinusculaAssignment_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Transformador__MinusculaAssignment_1_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getTransformadorAccess().getMinusculaAssignment_1_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalPlan2Cand.g:226:2: ( ( rule__Transformador__ApenasNumerosAssignment_1_2 ) )
                    {
                    // InternalPlan2Cand.g:226:2: ( ( rule__Transformador__ApenasNumerosAssignment_1_2 ) )
                    // InternalPlan2Cand.g:227:3: ( rule__Transformador__ApenasNumerosAssignment_1_2 )
                    {
                     before(grammarAccess.getTransformadorAccess().getApenasNumerosAssignment_1_2()); 
                    // InternalPlan2Cand.g:228:3: ( rule__Transformador__ApenasNumerosAssignment_1_2 )
                    // InternalPlan2Cand.g:228:4: rule__Transformador__ApenasNumerosAssignment_1_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Transformador__ApenasNumerosAssignment_1_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getTransformadorAccess().getApenasNumerosAssignment_1_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformador__Alternatives_1"


    // $ANTLR start "rule__TipoDado__Alternatives"
    // InternalPlan2Cand.g:236:1: rule__TipoDado__Alternatives : ( ( RULE_INT ) | ( RULE_CPF_FORMATADO ) | ( RULE_EMAIL ) | ( RULE_NOME ) | ( RULE_ID ) );
    public final void rule__TipoDado__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:240:1: ( ( RULE_INT ) | ( RULE_CPF_FORMATADO ) | ( RULE_EMAIL ) | ( RULE_NOME ) | ( RULE_ID ) )
            int alt3=5;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                alt3=1;
                }
                break;
            case RULE_CPF_FORMATADO:
                {
                alt3=2;
                }
                break;
            case RULE_EMAIL:
                {
                alt3=3;
                }
                break;
            case RULE_NOME:
                {
                alt3=4;
                }
                break;
            case RULE_ID:
                {
                alt3=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalPlan2Cand.g:241:2: ( RULE_INT )
                    {
                    // InternalPlan2Cand.g:241:2: ( RULE_INT )
                    // InternalPlan2Cand.g:242:3: RULE_INT
                    {
                     before(grammarAccess.getTipoDadoAccess().getINTTerminalRuleCall_0()); 
                    match(input,RULE_INT,FOLLOW_2); 
                     after(grammarAccess.getTipoDadoAccess().getINTTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPlan2Cand.g:247:2: ( RULE_CPF_FORMATADO )
                    {
                    // InternalPlan2Cand.g:247:2: ( RULE_CPF_FORMATADO )
                    // InternalPlan2Cand.g:248:3: RULE_CPF_FORMATADO
                    {
                     before(grammarAccess.getTipoDadoAccess().getCPF_FORMATADOTerminalRuleCall_1()); 
                    match(input,RULE_CPF_FORMATADO,FOLLOW_2); 
                     after(grammarAccess.getTipoDadoAccess().getCPF_FORMATADOTerminalRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalPlan2Cand.g:253:2: ( RULE_EMAIL )
                    {
                    // InternalPlan2Cand.g:253:2: ( RULE_EMAIL )
                    // InternalPlan2Cand.g:254:3: RULE_EMAIL
                    {
                     before(grammarAccess.getTipoDadoAccess().getEMAILTerminalRuleCall_2()); 
                    match(input,RULE_EMAIL,FOLLOW_2); 
                     after(grammarAccess.getTipoDadoAccess().getEMAILTerminalRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalPlan2Cand.g:259:2: ( RULE_NOME )
                    {
                    // InternalPlan2Cand.g:259:2: ( RULE_NOME )
                    // InternalPlan2Cand.g:260:3: RULE_NOME
                    {
                     before(grammarAccess.getTipoDadoAccess().getNOMETerminalRuleCall_3()); 
                    match(input,RULE_NOME,FOLLOW_2); 
                     after(grammarAccess.getTipoDadoAccess().getNOMETerminalRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalPlan2Cand.g:265:2: ( RULE_ID )
                    {
                    // InternalPlan2Cand.g:265:2: ( RULE_ID )
                    // InternalPlan2Cand.g:266:3: RULE_ID
                    {
                     before(grammarAccess.getTipoDadoAccess().getIDTerminalRuleCall_4()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getTipoDadoAccess().getIDTerminalRuleCall_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TipoDado__Alternatives"


    // $ANTLR start "rule__Model__Group__0"
    // InternalPlan2Cand.g:275:1: rule__Model__Group__0 : rule__Model__Group__0__Impl rule__Model__Group__1 ;
    public final void rule__Model__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:279:1: ( rule__Model__Group__0__Impl rule__Model__Group__1 )
            // InternalPlan2Cand.g:280:2: rule__Model__Group__0__Impl rule__Model__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Model__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0"


    // $ANTLR start "rule__Model__Group__0__Impl"
    // InternalPlan2Cand.g:287:1: rule__Model__Group__0__Impl : ( 'BEGIN_MAPEAMENTO' ) ;
    public final void rule__Model__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:291:1: ( ( 'BEGIN_MAPEAMENTO' ) )
            // InternalPlan2Cand.g:292:1: ( 'BEGIN_MAPEAMENTO' )
            {
            // InternalPlan2Cand.g:292:1: ( 'BEGIN_MAPEAMENTO' )
            // InternalPlan2Cand.g:293:2: 'BEGIN_MAPEAMENTO'
            {
             before(grammarAccess.getModelAccess().getBEGIN_MAPEAMENTOKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getBEGIN_MAPEAMENTOKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0__Impl"


    // $ANTLR start "rule__Model__Group__1"
    // InternalPlan2Cand.g:302:1: rule__Model__Group__1 : rule__Model__Group__1__Impl rule__Model__Group__2 ;
    public final void rule__Model__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:306:1: ( rule__Model__Group__1__Impl rule__Model__Group__2 )
            // InternalPlan2Cand.g:307:2: rule__Model__Group__1__Impl rule__Model__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Model__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1"


    // $ANTLR start "rule__Model__Group__1__Impl"
    // InternalPlan2Cand.g:314:1: rule__Model__Group__1__Impl : ( ( ( rule__Model__DefinicaoColunasAssignment_1 ) ) ( ( rule__Model__DefinicaoColunasAssignment_1 )* ) ) ;
    public final void rule__Model__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:318:1: ( ( ( ( rule__Model__DefinicaoColunasAssignment_1 ) ) ( ( rule__Model__DefinicaoColunasAssignment_1 )* ) ) )
            // InternalPlan2Cand.g:319:1: ( ( ( rule__Model__DefinicaoColunasAssignment_1 ) ) ( ( rule__Model__DefinicaoColunasAssignment_1 )* ) )
            {
            // InternalPlan2Cand.g:319:1: ( ( ( rule__Model__DefinicaoColunasAssignment_1 ) ) ( ( rule__Model__DefinicaoColunasAssignment_1 )* ) )
            // InternalPlan2Cand.g:320:2: ( ( rule__Model__DefinicaoColunasAssignment_1 ) ) ( ( rule__Model__DefinicaoColunasAssignment_1 )* )
            {
            // InternalPlan2Cand.g:320:2: ( ( rule__Model__DefinicaoColunasAssignment_1 ) )
            // InternalPlan2Cand.g:321:3: ( rule__Model__DefinicaoColunasAssignment_1 )
            {
             before(grammarAccess.getModelAccess().getDefinicaoColunasAssignment_1()); 
            // InternalPlan2Cand.g:322:3: ( rule__Model__DefinicaoColunasAssignment_1 )
            // InternalPlan2Cand.g:322:4: rule__Model__DefinicaoColunasAssignment_1
            {
            pushFollow(FOLLOW_6);
            rule__Model__DefinicaoColunasAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getDefinicaoColunasAssignment_1()); 

            }

            // InternalPlan2Cand.g:325:2: ( ( rule__Model__DefinicaoColunasAssignment_1 )* )
            // InternalPlan2Cand.g:326:3: ( rule__Model__DefinicaoColunasAssignment_1 )*
            {
             before(grammarAccess.getModelAccess().getDefinicaoColunasAssignment_1()); 
            // InternalPlan2Cand.g:327:3: ( rule__Model__DefinicaoColunasAssignment_1 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==20) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalPlan2Cand.g:327:4: rule__Model__DefinicaoColunasAssignment_1
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__Model__DefinicaoColunasAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getDefinicaoColunasAssignment_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1__Impl"


    // $ANTLR start "rule__Model__Group__2"
    // InternalPlan2Cand.g:336:1: rule__Model__Group__2 : rule__Model__Group__2__Impl rule__Model__Group__3 ;
    public final void rule__Model__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:340:1: ( rule__Model__Group__2__Impl rule__Model__Group__3 )
            // InternalPlan2Cand.g:341:2: rule__Model__Group__2__Impl rule__Model__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__Model__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2"


    // $ANTLR start "rule__Model__Group__2__Impl"
    // InternalPlan2Cand.g:348:1: rule__Model__Group__2__Impl : ( 'END_MAPEAMENTO' ) ;
    public final void rule__Model__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:352:1: ( ( 'END_MAPEAMENTO' ) )
            // InternalPlan2Cand.g:353:1: ( 'END_MAPEAMENTO' )
            {
            // InternalPlan2Cand.g:353:1: ( 'END_MAPEAMENTO' )
            // InternalPlan2Cand.g:354:2: 'END_MAPEAMENTO'
            {
             before(grammarAccess.getModelAccess().getEND_MAPEAMENTOKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getEND_MAPEAMENTOKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2__Impl"


    // $ANTLR start "rule__Model__Group__3"
    // InternalPlan2Cand.g:363:1: rule__Model__Group__3 : rule__Model__Group__3__Impl rule__Model__Group__4 ;
    public final void rule__Model__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:367:1: ( rule__Model__Group__3__Impl rule__Model__Group__4 )
            // InternalPlan2Cand.g:368:2: rule__Model__Group__3__Impl rule__Model__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__Model__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__3"


    // $ANTLR start "rule__Model__Group__3__Impl"
    // InternalPlan2Cand.g:375:1: rule__Model__Group__3__Impl : ( 'BEGIN_DADOS' ) ;
    public final void rule__Model__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:379:1: ( ( 'BEGIN_DADOS' ) )
            // InternalPlan2Cand.g:380:1: ( 'BEGIN_DADOS' )
            {
            // InternalPlan2Cand.g:380:1: ( 'BEGIN_DADOS' )
            // InternalPlan2Cand.g:381:2: 'BEGIN_DADOS'
            {
             before(grammarAccess.getModelAccess().getBEGIN_DADOSKeyword_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getBEGIN_DADOSKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__3__Impl"


    // $ANTLR start "rule__Model__Group__4"
    // InternalPlan2Cand.g:390:1: rule__Model__Group__4 : rule__Model__Group__4__Impl rule__Model__Group__5 ;
    public final void rule__Model__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:394:1: ( rule__Model__Group__4__Impl rule__Model__Group__5 )
            // InternalPlan2Cand.g:395:2: rule__Model__Group__4__Impl rule__Model__Group__5
            {
            pushFollow(FOLLOW_9);
            rule__Model__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__4"


    // $ANTLR start "rule__Model__Group__4__Impl"
    // InternalPlan2Cand.g:402:1: rule__Model__Group__4__Impl : ( ( rule__Model__TodosDadosAssignment_4 ) ) ;
    public final void rule__Model__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:406:1: ( ( ( rule__Model__TodosDadosAssignment_4 ) ) )
            // InternalPlan2Cand.g:407:1: ( ( rule__Model__TodosDadosAssignment_4 ) )
            {
            // InternalPlan2Cand.g:407:1: ( ( rule__Model__TodosDadosAssignment_4 ) )
            // InternalPlan2Cand.g:408:2: ( rule__Model__TodosDadosAssignment_4 )
            {
             before(grammarAccess.getModelAccess().getTodosDadosAssignment_4()); 
            // InternalPlan2Cand.g:409:2: ( rule__Model__TodosDadosAssignment_4 )
            // InternalPlan2Cand.g:409:3: rule__Model__TodosDadosAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Model__TodosDadosAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getTodosDadosAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__4__Impl"


    // $ANTLR start "rule__Model__Group__5"
    // InternalPlan2Cand.g:417:1: rule__Model__Group__5 : rule__Model__Group__5__Impl ;
    public final void rule__Model__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:421:1: ( rule__Model__Group__5__Impl )
            // InternalPlan2Cand.g:422:2: rule__Model__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__5"


    // $ANTLR start "rule__Model__Group__5__Impl"
    // InternalPlan2Cand.g:428:1: rule__Model__Group__5__Impl : ( 'END_DADOS' ) ;
    public final void rule__Model__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:432:1: ( ( 'END_DADOS' ) )
            // InternalPlan2Cand.g:433:1: ( 'END_DADOS' )
            {
            // InternalPlan2Cand.g:433:1: ( 'END_DADOS' )
            // InternalPlan2Cand.g:434:2: 'END_DADOS'
            {
             before(grammarAccess.getModelAccess().getEND_DADOSKeyword_5()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getEND_DADOSKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__5__Impl"


    // $ANTLR start "rule__DefinicaoColunas__Group__0"
    // InternalPlan2Cand.g:444:1: rule__DefinicaoColunas__Group__0 : rule__DefinicaoColunas__Group__0__Impl rule__DefinicaoColunas__Group__1 ;
    public final void rule__DefinicaoColunas__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:448:1: ( rule__DefinicaoColunas__Group__0__Impl rule__DefinicaoColunas__Group__1 )
            // InternalPlan2Cand.g:449:2: rule__DefinicaoColunas__Group__0__Impl rule__DefinicaoColunas__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__DefinicaoColunas__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DefinicaoColunas__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__0"


    // $ANTLR start "rule__DefinicaoColunas__Group__0__Impl"
    // InternalPlan2Cand.g:456:1: rule__DefinicaoColunas__Group__0__Impl : ( 'coluna' ) ;
    public final void rule__DefinicaoColunas__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:460:1: ( ( 'coluna' ) )
            // InternalPlan2Cand.g:461:1: ( 'coluna' )
            {
            // InternalPlan2Cand.g:461:1: ( 'coluna' )
            // InternalPlan2Cand.g:462:2: 'coluna'
            {
             before(grammarAccess.getDefinicaoColunasAccess().getColunaKeyword_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getDefinicaoColunasAccess().getColunaKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__0__Impl"


    // $ANTLR start "rule__DefinicaoColunas__Group__1"
    // InternalPlan2Cand.g:471:1: rule__DefinicaoColunas__Group__1 : rule__DefinicaoColunas__Group__1__Impl rule__DefinicaoColunas__Group__2 ;
    public final void rule__DefinicaoColunas__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:475:1: ( rule__DefinicaoColunas__Group__1__Impl rule__DefinicaoColunas__Group__2 )
            // InternalPlan2Cand.g:476:2: rule__DefinicaoColunas__Group__1__Impl rule__DefinicaoColunas__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__DefinicaoColunas__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DefinicaoColunas__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__1"


    // $ANTLR start "rule__DefinicaoColunas__Group__1__Impl"
    // InternalPlan2Cand.g:483:1: rule__DefinicaoColunas__Group__1__Impl : ( ( rule__DefinicaoColunas__ColunaAssignment_1 ) ) ;
    public final void rule__DefinicaoColunas__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:487:1: ( ( ( rule__DefinicaoColunas__ColunaAssignment_1 ) ) )
            // InternalPlan2Cand.g:488:1: ( ( rule__DefinicaoColunas__ColunaAssignment_1 ) )
            {
            // InternalPlan2Cand.g:488:1: ( ( rule__DefinicaoColunas__ColunaAssignment_1 ) )
            // InternalPlan2Cand.g:489:2: ( rule__DefinicaoColunas__ColunaAssignment_1 )
            {
             before(grammarAccess.getDefinicaoColunasAccess().getColunaAssignment_1()); 
            // InternalPlan2Cand.g:490:2: ( rule__DefinicaoColunas__ColunaAssignment_1 )
            // InternalPlan2Cand.g:490:3: rule__DefinicaoColunas__ColunaAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DefinicaoColunas__ColunaAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDefinicaoColunasAccess().getColunaAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__1__Impl"


    // $ANTLR start "rule__DefinicaoColunas__Group__2"
    // InternalPlan2Cand.g:498:1: rule__DefinicaoColunas__Group__2 : rule__DefinicaoColunas__Group__2__Impl rule__DefinicaoColunas__Group__3 ;
    public final void rule__DefinicaoColunas__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:502:1: ( rule__DefinicaoColunas__Group__2__Impl rule__DefinicaoColunas__Group__3 )
            // InternalPlan2Cand.g:503:2: rule__DefinicaoColunas__Group__2__Impl rule__DefinicaoColunas__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__DefinicaoColunas__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DefinicaoColunas__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__2"


    // $ANTLR start "rule__DefinicaoColunas__Group__2__Impl"
    // InternalPlan2Cand.g:510:1: rule__DefinicaoColunas__Group__2__Impl : ( ':' ) ;
    public final void rule__DefinicaoColunas__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:514:1: ( ( ':' ) )
            // InternalPlan2Cand.g:515:1: ( ':' )
            {
            // InternalPlan2Cand.g:515:1: ( ':' )
            // InternalPlan2Cand.g:516:2: ':'
            {
             before(grammarAccess.getDefinicaoColunasAccess().getColonKeyword_2()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getDefinicaoColunasAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__2__Impl"


    // $ANTLR start "rule__DefinicaoColunas__Group__3"
    // InternalPlan2Cand.g:525:1: rule__DefinicaoColunas__Group__3 : rule__DefinicaoColunas__Group__3__Impl rule__DefinicaoColunas__Group__4 ;
    public final void rule__DefinicaoColunas__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:529:1: ( rule__DefinicaoColunas__Group__3__Impl rule__DefinicaoColunas__Group__4 )
            // InternalPlan2Cand.g:530:2: rule__DefinicaoColunas__Group__3__Impl rule__DefinicaoColunas__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__DefinicaoColunas__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DefinicaoColunas__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__3"


    // $ANTLR start "rule__DefinicaoColunas__Group__3__Impl"
    // InternalPlan2Cand.g:537:1: rule__DefinicaoColunas__Group__3__Impl : ( 'destino' ) ;
    public final void rule__DefinicaoColunas__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:541:1: ( ( 'destino' ) )
            // InternalPlan2Cand.g:542:1: ( 'destino' )
            {
            // InternalPlan2Cand.g:542:1: ( 'destino' )
            // InternalPlan2Cand.g:543:2: 'destino'
            {
             before(grammarAccess.getDefinicaoColunasAccess().getDestinoKeyword_3()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getDefinicaoColunasAccess().getDestinoKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__3__Impl"


    // $ANTLR start "rule__DefinicaoColunas__Group__4"
    // InternalPlan2Cand.g:552:1: rule__DefinicaoColunas__Group__4 : rule__DefinicaoColunas__Group__4__Impl rule__DefinicaoColunas__Group__5 ;
    public final void rule__DefinicaoColunas__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:556:1: ( rule__DefinicaoColunas__Group__4__Impl rule__DefinicaoColunas__Group__5 )
            // InternalPlan2Cand.g:557:2: rule__DefinicaoColunas__Group__4__Impl rule__DefinicaoColunas__Group__5
            {
            pushFollow(FOLLOW_14);
            rule__DefinicaoColunas__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DefinicaoColunas__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__4"


    // $ANTLR start "rule__DefinicaoColunas__Group__4__Impl"
    // InternalPlan2Cand.g:564:1: rule__DefinicaoColunas__Group__4__Impl : ( '=' ) ;
    public final void rule__DefinicaoColunas__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:568:1: ( ( '=' ) )
            // InternalPlan2Cand.g:569:1: ( '=' )
            {
            // InternalPlan2Cand.g:569:1: ( '=' )
            // InternalPlan2Cand.g:570:2: '='
            {
             before(grammarAccess.getDefinicaoColunasAccess().getEqualsSignKeyword_4()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getDefinicaoColunasAccess().getEqualsSignKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__4__Impl"


    // $ANTLR start "rule__DefinicaoColunas__Group__5"
    // InternalPlan2Cand.g:579:1: rule__DefinicaoColunas__Group__5 : rule__DefinicaoColunas__Group__5__Impl rule__DefinicaoColunas__Group__6 ;
    public final void rule__DefinicaoColunas__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:583:1: ( rule__DefinicaoColunas__Group__5__Impl rule__DefinicaoColunas__Group__6 )
            // InternalPlan2Cand.g:584:2: rule__DefinicaoColunas__Group__5__Impl rule__DefinicaoColunas__Group__6
            {
            pushFollow(FOLLOW_15);
            rule__DefinicaoColunas__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DefinicaoColunas__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__5"


    // $ANTLR start "rule__DefinicaoColunas__Group__5__Impl"
    // InternalPlan2Cand.g:591:1: rule__DefinicaoColunas__Group__5__Impl : ( ( rule__DefinicaoColunas__DestinoAssignment_5 ) ) ;
    public final void rule__DefinicaoColunas__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:595:1: ( ( ( rule__DefinicaoColunas__DestinoAssignment_5 ) ) )
            // InternalPlan2Cand.g:596:1: ( ( rule__DefinicaoColunas__DestinoAssignment_5 ) )
            {
            // InternalPlan2Cand.g:596:1: ( ( rule__DefinicaoColunas__DestinoAssignment_5 ) )
            // InternalPlan2Cand.g:597:2: ( rule__DefinicaoColunas__DestinoAssignment_5 )
            {
             before(grammarAccess.getDefinicaoColunasAccess().getDestinoAssignment_5()); 
            // InternalPlan2Cand.g:598:2: ( rule__DefinicaoColunas__DestinoAssignment_5 )
            // InternalPlan2Cand.g:598:3: rule__DefinicaoColunas__DestinoAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__DefinicaoColunas__DestinoAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getDefinicaoColunasAccess().getDestinoAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__5__Impl"


    // $ANTLR start "rule__DefinicaoColunas__Group__6"
    // InternalPlan2Cand.g:606:1: rule__DefinicaoColunas__Group__6 : rule__DefinicaoColunas__Group__6__Impl rule__DefinicaoColunas__Group__7 ;
    public final void rule__DefinicaoColunas__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:610:1: ( rule__DefinicaoColunas__Group__6__Impl rule__DefinicaoColunas__Group__7 )
            // InternalPlan2Cand.g:611:2: rule__DefinicaoColunas__Group__6__Impl rule__DefinicaoColunas__Group__7
            {
            pushFollow(FOLLOW_15);
            rule__DefinicaoColunas__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DefinicaoColunas__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__6"


    // $ANTLR start "rule__DefinicaoColunas__Group__6__Impl"
    // InternalPlan2Cand.g:618:1: rule__DefinicaoColunas__Group__6__Impl : ( ( rule__DefinicaoColunas__TransformadorAssignment_6 )? ) ;
    public final void rule__DefinicaoColunas__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:622:1: ( ( ( rule__DefinicaoColunas__TransformadorAssignment_6 )? ) )
            // InternalPlan2Cand.g:623:1: ( ( rule__DefinicaoColunas__TransformadorAssignment_6 )? )
            {
            // InternalPlan2Cand.g:623:1: ( ( rule__DefinicaoColunas__TransformadorAssignment_6 )? )
            // InternalPlan2Cand.g:624:2: ( rule__DefinicaoColunas__TransformadorAssignment_6 )?
            {
             before(grammarAccess.getDefinicaoColunasAccess().getTransformadorAssignment_6()); 
            // InternalPlan2Cand.g:625:2: ( rule__DefinicaoColunas__TransformadorAssignment_6 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==25) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalPlan2Cand.g:625:3: rule__DefinicaoColunas__TransformadorAssignment_6
                    {
                    pushFollow(FOLLOW_2);
                    rule__DefinicaoColunas__TransformadorAssignment_6();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDefinicaoColunasAccess().getTransformadorAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__6__Impl"


    // $ANTLR start "rule__DefinicaoColunas__Group__7"
    // InternalPlan2Cand.g:633:1: rule__DefinicaoColunas__Group__7 : rule__DefinicaoColunas__Group__7__Impl ;
    public final void rule__DefinicaoColunas__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:637:1: ( rule__DefinicaoColunas__Group__7__Impl )
            // InternalPlan2Cand.g:638:2: rule__DefinicaoColunas__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DefinicaoColunas__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__7"


    // $ANTLR start "rule__DefinicaoColunas__Group__7__Impl"
    // InternalPlan2Cand.g:644:1: rule__DefinicaoColunas__Group__7__Impl : ( ';' ) ;
    public final void rule__DefinicaoColunas__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:648:1: ( ( ';' ) )
            // InternalPlan2Cand.g:649:1: ( ';' )
            {
            // InternalPlan2Cand.g:649:1: ( ';' )
            // InternalPlan2Cand.g:650:2: ';'
            {
             before(grammarAccess.getDefinicaoColunasAccess().getSemicolonKeyword_7()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getDefinicaoColunasAccess().getSemicolonKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__Group__7__Impl"


    // $ANTLR start "rule__Transformador__Group__0"
    // InternalPlan2Cand.g:660:1: rule__Transformador__Group__0 : rule__Transformador__Group__0__Impl rule__Transformador__Group__1 ;
    public final void rule__Transformador__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:664:1: ( rule__Transformador__Group__0__Impl rule__Transformador__Group__1 )
            // InternalPlan2Cand.g:665:2: rule__Transformador__Group__0__Impl rule__Transformador__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__Transformador__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transformador__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformador__Group__0"


    // $ANTLR start "rule__Transformador__Group__0__Impl"
    // InternalPlan2Cand.g:672:1: rule__Transformador__Group__0__Impl : ( ',' ) ;
    public final void rule__Transformador__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:676:1: ( ( ',' ) )
            // InternalPlan2Cand.g:677:1: ( ',' )
            {
            // InternalPlan2Cand.g:677:1: ( ',' )
            // InternalPlan2Cand.g:678:2: ','
            {
             before(grammarAccess.getTransformadorAccess().getCommaKeyword_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getTransformadorAccess().getCommaKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformador__Group__0__Impl"


    // $ANTLR start "rule__Transformador__Group__1"
    // InternalPlan2Cand.g:687:1: rule__Transformador__Group__1 : rule__Transformador__Group__1__Impl ;
    public final void rule__Transformador__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:691:1: ( rule__Transformador__Group__1__Impl )
            // InternalPlan2Cand.g:692:2: rule__Transformador__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transformador__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformador__Group__1"


    // $ANTLR start "rule__Transformador__Group__1__Impl"
    // InternalPlan2Cand.g:698:1: rule__Transformador__Group__1__Impl : ( ( rule__Transformador__Alternatives_1 ) ) ;
    public final void rule__Transformador__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:702:1: ( ( ( rule__Transformador__Alternatives_1 ) ) )
            // InternalPlan2Cand.g:703:1: ( ( rule__Transformador__Alternatives_1 ) )
            {
            // InternalPlan2Cand.g:703:1: ( ( rule__Transformador__Alternatives_1 ) )
            // InternalPlan2Cand.g:704:2: ( rule__Transformador__Alternatives_1 )
            {
             before(grammarAccess.getTransformadorAccess().getAlternatives_1()); 
            // InternalPlan2Cand.g:705:2: ( rule__Transformador__Alternatives_1 )
            // InternalPlan2Cand.g:705:3: rule__Transformador__Alternatives_1
            {
            pushFollow(FOLLOW_2);
            rule__Transformador__Alternatives_1();

            state._fsp--;


            }

             after(grammarAccess.getTransformadorAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformador__Group__1__Impl"


    // $ANTLR start "rule__LinhaDados__Group__0"
    // InternalPlan2Cand.g:714:1: rule__LinhaDados__Group__0 : rule__LinhaDados__Group__0__Impl rule__LinhaDados__Group__1 ;
    public final void rule__LinhaDados__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:718:1: ( rule__LinhaDados__Group__0__Impl rule__LinhaDados__Group__1 )
            // InternalPlan2Cand.g:719:2: rule__LinhaDados__Group__0__Impl rule__LinhaDados__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__LinhaDados__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LinhaDados__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LinhaDados__Group__0"


    // $ANTLR start "rule__LinhaDados__Group__0__Impl"
    // InternalPlan2Cand.g:726:1: rule__LinhaDados__Group__0__Impl : ( ( rule__LinhaDados__LinhaAssignment_0 ) ) ;
    public final void rule__LinhaDados__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:730:1: ( ( ( rule__LinhaDados__LinhaAssignment_0 ) ) )
            // InternalPlan2Cand.g:731:1: ( ( rule__LinhaDados__LinhaAssignment_0 ) )
            {
            // InternalPlan2Cand.g:731:1: ( ( rule__LinhaDados__LinhaAssignment_0 ) )
            // InternalPlan2Cand.g:732:2: ( rule__LinhaDados__LinhaAssignment_0 )
            {
             before(grammarAccess.getLinhaDadosAccess().getLinhaAssignment_0()); 
            // InternalPlan2Cand.g:733:2: ( rule__LinhaDados__LinhaAssignment_0 )
            // InternalPlan2Cand.g:733:3: rule__LinhaDados__LinhaAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__LinhaDados__LinhaAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getLinhaDadosAccess().getLinhaAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LinhaDados__Group__0__Impl"


    // $ANTLR start "rule__LinhaDados__Group__1"
    // InternalPlan2Cand.g:741:1: rule__LinhaDados__Group__1 : rule__LinhaDados__Group__1__Impl ;
    public final void rule__LinhaDados__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:745:1: ( rule__LinhaDados__Group__1__Impl )
            // InternalPlan2Cand.g:746:2: rule__LinhaDados__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LinhaDados__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LinhaDados__Group__1"


    // $ANTLR start "rule__LinhaDados__Group__1__Impl"
    // InternalPlan2Cand.g:752:1: rule__LinhaDados__Group__1__Impl : ( ( rule__LinhaDados__Group_1__0 )* ) ;
    public final void rule__LinhaDados__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:756:1: ( ( ( rule__LinhaDados__Group_1__0 )* ) )
            // InternalPlan2Cand.g:757:1: ( ( rule__LinhaDados__Group_1__0 )* )
            {
            // InternalPlan2Cand.g:757:1: ( ( rule__LinhaDados__Group_1__0 )* )
            // InternalPlan2Cand.g:758:2: ( rule__LinhaDados__Group_1__0 )*
            {
             before(grammarAccess.getLinhaDadosAccess().getGroup_1()); 
            // InternalPlan2Cand.g:759:2: ( rule__LinhaDados__Group_1__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==RULE_SEPARADOR) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalPlan2Cand.g:759:3: rule__LinhaDados__Group_1__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__LinhaDados__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getLinhaDadosAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LinhaDados__Group__1__Impl"


    // $ANTLR start "rule__LinhaDados__Group_1__0"
    // InternalPlan2Cand.g:768:1: rule__LinhaDados__Group_1__0 : rule__LinhaDados__Group_1__0__Impl rule__LinhaDados__Group_1__1 ;
    public final void rule__LinhaDados__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:772:1: ( rule__LinhaDados__Group_1__0__Impl rule__LinhaDados__Group_1__1 )
            // InternalPlan2Cand.g:773:2: rule__LinhaDados__Group_1__0__Impl rule__LinhaDados__Group_1__1
            {
            pushFollow(FOLLOW_8);
            rule__LinhaDados__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LinhaDados__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LinhaDados__Group_1__0"


    // $ANTLR start "rule__LinhaDados__Group_1__0__Impl"
    // InternalPlan2Cand.g:780:1: rule__LinhaDados__Group_1__0__Impl : ( RULE_SEPARADOR ) ;
    public final void rule__LinhaDados__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:784:1: ( ( RULE_SEPARADOR ) )
            // InternalPlan2Cand.g:785:1: ( RULE_SEPARADOR )
            {
            // InternalPlan2Cand.g:785:1: ( RULE_SEPARADOR )
            // InternalPlan2Cand.g:786:2: RULE_SEPARADOR
            {
             before(grammarAccess.getLinhaDadosAccess().getSEPARADORTerminalRuleCall_1_0()); 
            match(input,RULE_SEPARADOR,FOLLOW_2); 
             after(grammarAccess.getLinhaDadosAccess().getSEPARADORTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LinhaDados__Group_1__0__Impl"


    // $ANTLR start "rule__LinhaDados__Group_1__1"
    // InternalPlan2Cand.g:795:1: rule__LinhaDados__Group_1__1 : rule__LinhaDados__Group_1__1__Impl ;
    public final void rule__LinhaDados__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:799:1: ( rule__LinhaDados__Group_1__1__Impl )
            // InternalPlan2Cand.g:800:2: rule__LinhaDados__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LinhaDados__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LinhaDados__Group_1__1"


    // $ANTLR start "rule__LinhaDados__Group_1__1__Impl"
    // InternalPlan2Cand.g:806:1: rule__LinhaDados__Group_1__1__Impl : ( ( rule__LinhaDados__LinhaAssignment_1_1 ) ) ;
    public final void rule__LinhaDados__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:810:1: ( ( ( rule__LinhaDados__LinhaAssignment_1_1 ) ) )
            // InternalPlan2Cand.g:811:1: ( ( rule__LinhaDados__LinhaAssignment_1_1 ) )
            {
            // InternalPlan2Cand.g:811:1: ( ( rule__LinhaDados__LinhaAssignment_1_1 ) )
            // InternalPlan2Cand.g:812:2: ( rule__LinhaDados__LinhaAssignment_1_1 )
            {
             before(grammarAccess.getLinhaDadosAccess().getLinhaAssignment_1_1()); 
            // InternalPlan2Cand.g:813:2: ( rule__LinhaDados__LinhaAssignment_1_1 )
            // InternalPlan2Cand.g:813:3: rule__LinhaDados__LinhaAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__LinhaDados__LinhaAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getLinhaDadosAccess().getLinhaAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LinhaDados__Group_1__1__Impl"


    // $ANTLR start "rule__Model__DefinicaoColunasAssignment_1"
    // InternalPlan2Cand.g:822:1: rule__Model__DefinicaoColunasAssignment_1 : ( ruleDefinicaoColunas ) ;
    public final void rule__Model__DefinicaoColunasAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:826:1: ( ( ruleDefinicaoColunas ) )
            // InternalPlan2Cand.g:827:2: ( ruleDefinicaoColunas )
            {
            // InternalPlan2Cand.g:827:2: ( ruleDefinicaoColunas )
            // InternalPlan2Cand.g:828:3: ruleDefinicaoColunas
            {
             before(grammarAccess.getModelAccess().getDefinicaoColunasDefinicaoColunasParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDefinicaoColunas();

            state._fsp--;

             after(grammarAccess.getModelAccess().getDefinicaoColunasDefinicaoColunasParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__DefinicaoColunasAssignment_1"


    // $ANTLR start "rule__Model__TodosDadosAssignment_4"
    // InternalPlan2Cand.g:837:1: rule__Model__TodosDadosAssignment_4 : ( ruleDados ) ;
    public final void rule__Model__TodosDadosAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:841:1: ( ( ruleDados ) )
            // InternalPlan2Cand.g:842:2: ( ruleDados )
            {
            // InternalPlan2Cand.g:842:2: ( ruleDados )
            // InternalPlan2Cand.g:843:3: ruleDados
            {
             before(grammarAccess.getModelAccess().getTodosDadosDadosParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleDados();

            state._fsp--;

             after(grammarAccess.getModelAccess().getTodosDadosDadosParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__TodosDadosAssignment_4"


    // $ANTLR start "rule__Dados__DadosAssignment"
    // InternalPlan2Cand.g:852:1: rule__Dados__DadosAssignment : ( ruleLinhaDados ) ;
    public final void rule__Dados__DadosAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:856:1: ( ( ruleLinhaDados ) )
            // InternalPlan2Cand.g:857:2: ( ruleLinhaDados )
            {
            // InternalPlan2Cand.g:857:2: ( ruleLinhaDados )
            // InternalPlan2Cand.g:858:3: ruleLinhaDados
            {
             before(grammarAccess.getDadosAccess().getDadosLinhaDadosParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleLinhaDados();

            state._fsp--;

             after(grammarAccess.getDadosAccess().getDadosLinhaDadosParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dados__DadosAssignment"


    // $ANTLR start "rule__DefinicaoColunas__ColunaAssignment_1"
    // InternalPlan2Cand.g:867:1: rule__DefinicaoColunas__ColunaAssignment_1 : ( RULE_INT ) ;
    public final void rule__DefinicaoColunas__ColunaAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:871:1: ( ( RULE_INT ) )
            // InternalPlan2Cand.g:872:2: ( RULE_INT )
            {
            // InternalPlan2Cand.g:872:2: ( RULE_INT )
            // InternalPlan2Cand.g:873:3: RULE_INT
            {
             before(grammarAccess.getDefinicaoColunasAccess().getColunaINTTerminalRuleCall_1_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getDefinicaoColunasAccess().getColunaINTTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__ColunaAssignment_1"


    // $ANTLR start "rule__DefinicaoColunas__DestinoAssignment_5"
    // InternalPlan2Cand.g:882:1: rule__DefinicaoColunas__DestinoAssignment_5 : ( RULE_COLUNA_BANCO ) ;
    public final void rule__DefinicaoColunas__DestinoAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:886:1: ( ( RULE_COLUNA_BANCO ) )
            // InternalPlan2Cand.g:887:2: ( RULE_COLUNA_BANCO )
            {
            // InternalPlan2Cand.g:887:2: ( RULE_COLUNA_BANCO )
            // InternalPlan2Cand.g:888:3: RULE_COLUNA_BANCO
            {
             before(grammarAccess.getDefinicaoColunasAccess().getDestinoCOLUNA_BANCOTerminalRuleCall_5_0()); 
            match(input,RULE_COLUNA_BANCO,FOLLOW_2); 
             after(grammarAccess.getDefinicaoColunasAccess().getDestinoCOLUNA_BANCOTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__DestinoAssignment_5"


    // $ANTLR start "rule__DefinicaoColunas__TransformadorAssignment_6"
    // InternalPlan2Cand.g:897:1: rule__DefinicaoColunas__TransformadorAssignment_6 : ( ruleTransformador ) ;
    public final void rule__DefinicaoColunas__TransformadorAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:901:1: ( ( ruleTransformador ) )
            // InternalPlan2Cand.g:902:2: ( ruleTransformador )
            {
            // InternalPlan2Cand.g:902:2: ( ruleTransformador )
            // InternalPlan2Cand.g:903:3: ruleTransformador
            {
             before(grammarAccess.getDefinicaoColunasAccess().getTransformadorTransformadorParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleTransformador();

            state._fsp--;

             after(grammarAccess.getDefinicaoColunasAccess().getTransformadorTransformadorParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefinicaoColunas__TransformadorAssignment_6"


    // $ANTLR start "rule__Transformador__MaiusculaAssignment_1_0"
    // InternalPlan2Cand.g:912:1: rule__Transformador__MaiusculaAssignment_1_0 : ( ( 'maiusculo' ) ) ;
    public final void rule__Transformador__MaiusculaAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:916:1: ( ( ( 'maiusculo' ) ) )
            // InternalPlan2Cand.g:917:2: ( ( 'maiusculo' ) )
            {
            // InternalPlan2Cand.g:917:2: ( ( 'maiusculo' ) )
            // InternalPlan2Cand.g:918:3: ( 'maiusculo' )
            {
             before(grammarAccess.getTransformadorAccess().getMaiusculaMaiusculoKeyword_1_0_0()); 
            // InternalPlan2Cand.g:919:3: ( 'maiusculo' )
            // InternalPlan2Cand.g:920:4: 'maiusculo'
            {
             before(grammarAccess.getTransformadorAccess().getMaiusculaMaiusculoKeyword_1_0_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getTransformadorAccess().getMaiusculaMaiusculoKeyword_1_0_0()); 

            }

             after(grammarAccess.getTransformadorAccess().getMaiusculaMaiusculoKeyword_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformador__MaiusculaAssignment_1_0"


    // $ANTLR start "rule__Transformador__MinusculaAssignment_1_1"
    // InternalPlan2Cand.g:931:1: rule__Transformador__MinusculaAssignment_1_1 : ( ( 'minusculo' ) ) ;
    public final void rule__Transformador__MinusculaAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:935:1: ( ( ( 'minusculo' ) ) )
            // InternalPlan2Cand.g:936:2: ( ( 'minusculo' ) )
            {
            // InternalPlan2Cand.g:936:2: ( ( 'minusculo' ) )
            // InternalPlan2Cand.g:937:3: ( 'minusculo' )
            {
             before(grammarAccess.getTransformadorAccess().getMinusculaMinusculoKeyword_1_1_0()); 
            // InternalPlan2Cand.g:938:3: ( 'minusculo' )
            // InternalPlan2Cand.g:939:4: 'minusculo'
            {
             before(grammarAccess.getTransformadorAccess().getMinusculaMinusculoKeyword_1_1_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getTransformadorAccess().getMinusculaMinusculoKeyword_1_1_0()); 

            }

             after(grammarAccess.getTransformadorAccess().getMinusculaMinusculoKeyword_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformador__MinusculaAssignment_1_1"


    // $ANTLR start "rule__Transformador__ApenasNumerosAssignment_1_2"
    // InternalPlan2Cand.g:950:1: rule__Transformador__ApenasNumerosAssignment_1_2 : ( ( 'apenas_numeros' ) ) ;
    public final void rule__Transformador__ApenasNumerosAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:954:1: ( ( ( 'apenas_numeros' ) ) )
            // InternalPlan2Cand.g:955:2: ( ( 'apenas_numeros' ) )
            {
            // InternalPlan2Cand.g:955:2: ( ( 'apenas_numeros' ) )
            // InternalPlan2Cand.g:956:3: ( 'apenas_numeros' )
            {
             before(grammarAccess.getTransformadorAccess().getApenasNumerosApenas_numerosKeyword_1_2_0()); 
            // InternalPlan2Cand.g:957:3: ( 'apenas_numeros' )
            // InternalPlan2Cand.g:958:4: 'apenas_numeros'
            {
             before(grammarAccess.getTransformadorAccess().getApenasNumerosApenas_numerosKeyword_1_2_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getTransformadorAccess().getApenasNumerosApenas_numerosKeyword_1_2_0()); 

            }

             after(grammarAccess.getTransformadorAccess().getApenasNumerosApenas_numerosKeyword_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transformador__ApenasNumerosAssignment_1_2"


    // $ANTLR start "rule__LinhaDados__LinhaAssignment_0"
    // InternalPlan2Cand.g:969:1: rule__LinhaDados__LinhaAssignment_0 : ( ruleTipoDado ) ;
    public final void rule__LinhaDados__LinhaAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:973:1: ( ( ruleTipoDado ) )
            // InternalPlan2Cand.g:974:2: ( ruleTipoDado )
            {
            // InternalPlan2Cand.g:974:2: ( ruleTipoDado )
            // InternalPlan2Cand.g:975:3: ruleTipoDado
            {
             before(grammarAccess.getLinhaDadosAccess().getLinhaTipoDadoParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleTipoDado();

            state._fsp--;

             after(grammarAccess.getLinhaDadosAccess().getLinhaTipoDadoParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LinhaDados__LinhaAssignment_0"


    // $ANTLR start "rule__LinhaDados__LinhaAssignment_1_1"
    // InternalPlan2Cand.g:984:1: rule__LinhaDados__LinhaAssignment_1_1 : ( ruleTipoDado ) ;
    public final void rule__LinhaDados__LinhaAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPlan2Cand.g:988:1: ( ( ruleTipoDado ) )
            // InternalPlan2Cand.g:989:2: ( ruleTipoDado )
            {
            // InternalPlan2Cand.g:989:2: ( ruleTipoDado )
            // InternalPlan2Cand.g:990:3: ruleTipoDado
            {
             before(grammarAccess.getLinhaDadosAccess().getLinhaTipoDadoParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTipoDado();

            state._fsp--;

             after(grammarAccess.getLinhaDadosAccess().getLinhaTipoDadoParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LinhaDados__LinhaAssignment_1_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00000000000001F2L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x00000000000001F0L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000003000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000001C000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000202L});

}