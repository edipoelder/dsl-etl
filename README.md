DSL-ETL - Uma DSL para Exportar, Transformar e Carregar dados
=============================================================

Este projeto faz parte de uma disciplina acadêmica sobre DSL, ministrada no Programa de Pós-Graduação em Tecnologia da Informação - IMD/UFRN,
desenvolvida utilizando XText.

Autores:
- Édipo Elder F. de Melo
- Iuri Janmichel de S. Lima

Organização
-----------

A linguagem, nomeada inicialmente de Plan2Cand, proposta no pacote br.ufrn.comperve.
Para rodar a aplicação Eclipse, o usuário deve criar um arquivo com a extensão .cetl.

Esta primeira versão tem um escopo reduzido, importando dados (inscrição, nome, CPF) de uma planilha para
a tabela Candidato, permitindo algumas transformações (converter em maiúscula ou minúscula, e remover pontos e traço do CPF).

## Gramática ##

Pra agilizar o desenvolvimento do projeto, definimos que a definição dos dados de ETL e os dados a serem importados
ficariam no mesmo arquivo. 

Desta forma, a linguagem trabalha em dois blocos: o primeiro definindo qual coluna da planilha corresponde a qual campo
no banco de dados, opcionalmente especificando transformações nos dados; e o segundo bloco contendo os dados copiados da planilha
em um formato CSV (separado por tabulação e com aspas entre strings).

[Gramática](https://gitlab.com/edipoelder/dsl-etl/-/blob/master/br.comperve.ufrn.cetl/src/br/comperve/ufrn/CETL.xtext)

## Geração do SQL ##

Como saída compilada da nossa gramática, criamos um arquivo SQL que poderá ser utilizado para carregamento dos dados 
diretamente no banco de dados.

O gerador de código, inicialmente, mapeia o índice da coluna da planilha em um campo no banco de dados, e cria um mapa com
os transformadores a serem aplicados.

[Gerador](https://gitlab.com/edipoelder/dsl-etl/-/blob/master/br.comperve.ufrn.cetl/src/br/comperve/xtext/candidato_import/generator/Plan2CandGenerator.xtend)

O projeto previa abrir uma conexão com o SGBD, e executar os comandos SQL remotamente. Contudo, dado ao curto tempo
para execução do projeto, reduzimos o requisito para apenas a geração do arquivo SQL.

## Resultados Obtidos ##

Percebemos que a construção de uma DSL é bastante útil para transformar dados simples em dados mais complexos. No nosso caso,
uma DSL para transformar dados vindo de planilhas para um formato de importação rápida no banco de dados agiliza o tempo 
dispendido na tarefa e simplifica sua complexidade, podendo ser realizada por usuários menos técnicos.

## Sugestões Futuras ##

A validação os dados a serem transformados não foi implementada neste momento. Porém, conforme vimos na documentação disponível,
não é difícil de ser implementada.

Outra sugestão dada é a de indicar um arquivo CSV, externo, onde os dados seriam lidos ao invés de copiá-los.
